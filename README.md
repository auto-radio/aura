<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-logo-landscape.png" width="300" vspace="20" />

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/aura/-/badges/release.svg)](https://gitlab.servus.at/aura/aura/-/releases) [![pipeline status](https://gitlab.servus.at/aura/aura/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/aura/-/commits/main) [![Documentation Status](https://readthedocs.org/projects/aura-automated-radio/badge/?version=latest)](https://aura-automated-radio.readthedocs.io/en/latest/?badge=latest)

# Automated Radio (AURA)

AURA is a free software suite for community radio stations. All code is Open Source and licensed
under AGPL 3.0.

## Getting started

- Website: [aura.radio](https://aura.radio/)
- Documentation: [docs.aura.radio](https://docs.aura.radio/)

## This repository holds:

- The Docker Compose bundles for AURA Web and AURA Playout, located in `config`. Check out the documentation at [docs.aura.radio](https://docs.aura.radio/) on how to use it
- Media assets to be used in various parts of the project, located in `assets`
- The complete documentation, located in `docs` and hosted at [docs.aura.radio](https://docs.aura.radio/)

## License

- All source code is licensed under [GNU Affero General Public License (AGPL) v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
- All other assets and text are licensed under [Creative Commons BY-NC-SA v3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/).
- Logos and trademarks of sponsors and supporters are copyrighted by the respective owners. They should be removed if you fork this repository.

These licenses apply unless stated differently.
