# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Ability to [load system settings with sample and custom fixtures](https://docs.aura.radio/en/latest/administration/aura-web.html#applying-radio-station-settings-with-fixtures).

### Changed

- Sample configuration override for `engine-api` is now in `YAML` format, instead of `INI`.
- Update `engine-recorder` sample configuration for override.

- Update `aura-web`. Nginx is now rootless. this improves security and fix some other permissions bug.
- There is manual interaction after updating, to fix permission problems. For that run:
```bash
    make aura-web.permissions-update
```

### Deprecated

- ...

### Removed

- ...

### Fixed

- Allow optional trailing slash for `/trackservice` endpoints and don't redirect (engine-api#25).
- Documentation: Replace `AURA_RECORDER_AUDIO_STORE_HOST` with `AURA_AUDIO_STORE`, (engine-recorder#39)

### Security

- ...

## [1.0.0-alpha2] - 2023-06-29

### Added

- Add development target `make dev.merge-changelog` to create a merged changelog, helping to prepare release notes.
- Add release notes section at [docs.aura.radio](https://docs.aura.radio)
- Add Tank Cut & Glue API to [api.aura.radio](https://api.aura.radio/).
- Add default [Audio Store](https://docs.aura.radio/en/latest/administration/deployment-preparation.html#setting-up-the-audio-store) directory `/var/audio/import` used for audio file imports via filesystem (aura#172).
- Add default setting for `AURA_AUDIO_STORE_RECORDINGS` to the playout `sample.env` file.
- Add audio targets for the Makefile to start and disable PulseAudio.
- Add optional engine-recorder profile to aura-playout.
- Add an log file for NGINX, aura-web (#147).
- Documentation: Add more details to the [Radio Station Administration](https://docs.aura.radio/en/latest/user/station-administration.html) chapter.
- Documentation: Add chapter on deleting and deactivating users in the User Guide.
- Documentation: Add more info on switching between releases to the Administration Guide.
- Add directories to `logs` and `audio` to avoid permission errors (#209)

### Changed

- Update sample configuration files for services in `config/service/sample-config`.
- Rename environment variable `AURA_ENGINE_SERVER_TIMEOUT` to `AURA_ENGINE_SERVER_TIMEOUT` in playout `sample.env`.
- Increase default value for `AURA_ENGINE_SERVER_TIMEOUT` in playout `sample.env`, to avoid malfunctions when idle (aura#165).
- Documentation: Update Contribution Guidelines and Coding Conventions in the Developer Guide.
- Documentation: Extend section for developers on performing releases.
- Change `/trackservice` endpoints to `/engine/api/v1/trackservice` and `/engine/api/v1/trackservice/current` according to API namespace conventions (aura#190).

### Deprecated

- Nested API endpoints in Steering and Tank are now deprecated. Compare the specification at api.aura.radio

### Fixed

- Fix an issue where `make aura-user.add` causes an unrelated Python error, raised from another command (#183).
- Fix the location of the Docker Compose bundles in the README.
- Fix an issue where `/trackservice` was not reachable (#159).
- Fix an issue where some settings in the `.env` and `.yml` where not applied correctly (#160).
- Fix an issue with the `aura-playout` targets in the Makefile (#168).
- Documentation: Replace old "meta" repository references with "aura".

## [1.0.0-alpha1] - 2023-03-02

Initial release.
