# Audio targets for AURA Makefiles


# Help

define audio_help
	@echo "    audio.pa.start  	        - start pulseaudio server"
	@echo "    audio.pa.stop   	        - stop pulseaudio server"
	@echo "    audio.pa.status 	        - status of pulseaudio server"
	@echo "    audio.pa.ctrl   	        - ui for pulseaudio server"
	@echo "    audio.alsa.cards	        - list alsa cards"
	@echo "    audio.alsa.test 	        - test configured audio device"
	@echo "    audio.alsa.copy 	        - copy asound.conf to ~/.asoundrc"
endef


# Targets


audio.pa.start::
	systemctl --user start pulseaudio.socket
	systemctl --user start pulseaudio.service

audio.pa.stop::
	systemctl --user stop pulseaudio.socket
	systemctl --user stop pulseaudio.service

audio.pa.status::
	pactl list

audio.pa.ctrl::
	pavucontrol

audio.alsa.cards::
	cat /proc/asound/cards

audio.alsa.test::
	speaker-test -D aura_engine

audio.alsa.copy::
	cp -u config/aura-playout/asound.conf ~/.asoundrc