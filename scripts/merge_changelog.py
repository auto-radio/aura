#!/usr/bin/env python3
#
# AURA (https://aura.radio)
#
# Copyright (C) 2023 - now() - The Aura Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os

import tomli

# Configuration

workdir = os.environ.get("AURA_WORK_DIR", ".tmp")
toml = tomli.load(open("pyproject.toml", "rb"))
version = toml["tool"]["poetry"]["version"]
os.environ['AURA_VERSION'] = version

services = [
    ["aura", "AURA_VERSION"],
    ["steering", "AURA_STEERING_VERSION"],
    ["tank", "AURA_TANK_VERSION"],
    ["tank-cut-glue", "AURA_TANK_CUT_GLUE_VERSION"],
    ["dashboard", "AURA_DASHBOARD_VERSION"],
    ["dashboard-clock", "AURA_DASHBOARD_CLOCK_VERSION"],
    ["engine", "AURA_ENGINE_VERSION"],
    ["engine-api", "AURA_ENGINE_API_VERSION"],
    ["engine-core", "AURA_ENGINE_CORE_VERSION"],
    ["engine-recorder", "AURA_ENGINE_RECORDER_VERSION"],
]
changelog = [
    ["", []],
    ["### Added", []],
    ["### Changed", []],
    ["### Deprecated", []],
    ["### Removed", []],
    ["### Fixed", []],
    ["### Security", []],
]


# Functions to read and write changelogs

def read_file(service, file):
    section_idx = 0
    service_version = os.environ.get(service[1], None)
    if service_version == "unstable":
        service_version = "Unreleased"
    print(f"\nProcessing [{service[0]} - {service_version}]")
    found_version = False

    for line in file:
        line = line.strip()

        if "## [" + service_version in line:
            found_version = True
            if 0 == len(changelog[0][1]):
                changelog[0][1].append(line + "\n")
            continue

        if found_version:
            # End of changelogs for this version
            if line.startswith("## "):
                print("Done")
                break
            # New section within version
            elif line.startswith("### "):
                msg = f"End of section '{changelog[section_idx][0]}' "
                msg += f"with {len(changelog[section_idx][1])} entries"
                print(msg)

                for i, section in enumerate(changelog[1:]):
                    if line.startswith(section[0]):
                        section_idx = i+1
                        break
                msg = f"\nStart of section '{changelog[section_idx][0]}' "
                print(msg)
                continue
            # Content of section
            elif line and "- ..." not in line:
                content = service[0] + ": " + line.replace("- ", "")
                if section_idx > 0:
                    content = "- " + content
                changelog[section_idx][1].append(content)


def write_file(changelog, file):
    for section_idx in changelog:
        if 0 == len(section_idx[1]):
            continue
        file.write("\n" + section_idx[0] + "\n\n")
        for line in section_idx[1]:
            file.write(line + "\n")


# Merge changelogs
print("Merging changelogs for AURA v" + version)
for service in services:
    with open(f"{workdir}/CHANGELOG-{service[0]}.md", "r") as file:
        read_file(service, file)

# Render combined changelog
output_filename = f"{workdir}/merged-CHANGELOG-{version}.md"
with open(output_filename, "w") as file:
    write_file(changelog, file)
print(f"\n---\nRendered combined changelog to '{output_filename}'\n")
