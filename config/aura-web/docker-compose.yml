version: "3.4"

services:

  steering-postgres:
    container_name: steering-postgres
    image: postgres:${POSTGRES_VERSION:-14}
    restart: always
    environment:
      TZ: ${TIMEZONE}
      POSTGRES_PASSWORD: ${AURA_STEERING_DB_PASS:?password for steering DB}
      POSTGRES_USER: ${AURA_STEERING_DB_USER:?database user for steering DB}
      POSTGRES_DB: ${AURA_STEERING_DB_NAME:?name of the database for steering}
    volumes:
      - steering_db_data:/var/lib/postgresql/data
    expose:
      - "5432"
    networks:
      - internal-steering
    healthcheck:
      test:
        [
          "CMD",
          "pg_isready",
          "-U",
          "${AURA_STEERING_DB_USER:?database user for steering DB}",
          "-d",
          "${AURA_STEERING_DB_NAME:?name of the database for steering}"
        ]

  steering:
    container_name: steering
    image: autoradio/steering:${AURA_STEERING_VERSION}
    depends_on:
      steering-postgres:
        condition: service_healthy
    user: "2872:2872"
    environment:
      TZ: ${TIMEZONE}
      DATABASE_ENGINE: ${AURA_STEERING_DB_ENGINE:-postgresql}
      POSTGRES_PASSWORD: ${AURA_STEERING_DB_PASS:?password for steering DB}
      POSTGRES_USER: ${AURA_STEERING_DB_USER:?database user for steering DB}
      POSTGRES_DB: ${AURA_STEERING_DB_NAME:?name of the database for steering}
      SECRET_KEY: ${AURA_STEERING_SECRET_KEY:?secret key for steering}
      DJANGO_SUPERUSER_USERNAME: ${AURA_STEERING_SUPERUSER_USERNAME:?username for django admin}
      DJANGO_SUPERUSER_PASSWORD: ${AURA_STEERING_SUPERUSER_PASSWORD:?password for django admin}
      DJANGO_SUPERUSER_EMAIL: ${AURA_STEERING_SUPERUSER_EMAIL:?email for django admin}
      ALLOWED_HOSTS: "localhost,127.0.0.1,steering,${AURA_HOST_NAME}"
      TANK_OIDC_CLIENT_ID: ${AURA_TANK_OIDC_CLIENT_ID:?client id for tank}
      TANK_OIDC_CLIENT_SECRET: ${AURA_TANK_OIDC_CLIENT_SECRET:?client secret for tank}
      AURA_HOST: ${AURA_HOST_NAME:?host domain of this aura deployment}
      AURA_PROTO: ${AURA_HOST_PROTO:-https}
      DASHBOARD_OIDC_CLIENT_ID: ${AURA_DASHBOARD_OIDC_CLIENT_ID:?dashboard client id}
      DASHBOARD_OIDC_CLIENT_SECRET: ${AURA_DASHBOARD_OIDC_CLIENT_SECRET:?client secret for dashboard}
      TANK_CALLBACK_BASE_URL: ${AURA_TANK_CALLBACK_BASE_URL}
      DASHBOARD_CALLBACK_BASE_URL: ${AURA_DASHBOARD_CALLBACK_BASE_URL}
      USE_LDAP_AUTH: ${AURA_STEERING_AUTH_LDAP_ENABLE}
      AUTH_LDAP_BIND_PASSWORD: ${AURA_STEERING_AUTH_LDAP_BIND_PASSWORD}
      STEERING_LOG_LEVEL: ${AURA_STEERING_LOG_LEVEL:-INFO}
    volumes:
      # Uncomment as soon Steering provides an overwrite logic
      # @see https://gitlab.servus.at/aura/aura/-/issues/132#note_7497
      # - ./../services/:/etc/aura/:ro
      - steering_static:/app/static
      - steering_media:/app/site_media
      - ${AURA_LOGS}:/app/logs
      - ./fixtures/custom:/app/fixtures/custom
    expose:
      - "8000"
    networks:
      - auranet
      - internal-steering
    healthcheck:
      test:
        [
          "CMD",
          "curl",
          "-o",
          "/dev/null",
          "http://localhost:8000/api/v1/"
        ]

  tank-postgres:
    container_name: tank-postgres
    image: postgres:${POSTGRES_VERSION:-14}
    restart: always
    environment:
      TZ: ${TIMEZONE}
      POSTGRES_PASSWORD: ${AURA_TANK_DB_PASS:?password for tank DB}
      POSTGRES_USER: ${AURA_TANK_DB_USER:?database user for tank DB}
      POSTGRES_DB: ${AURA_TANK_DB_NAME:?name of the database for tank}
    volumes:
      - tank_db_data:/var/lib/postgresql/data
    expose:
      - "5432"
    networks:
      - internal-tank
    healthcheck:
      test:
        [
          "CMD",
          "pg_isready",
          "-U",
          "${AURA_TANK_DB_USER:?database user for steering DB}",
          "-d",
          "${AURA_TANK_DB_NAME:?name of the database for steering}"
        ]

  tank:
    container_name: tank
    image: autoradio/tank:${AURA_TANK_VERSION}
    depends_on:
      steering:
        condition: service_healthy
      tank-postgres:
        condition: service_healthy
    user: "2872:2872"
    environment:
      TZ: ${TIMEZONE}
      TANK_DB_HOST: tank-postgres
      TANK_DB_NAME: ${AURA_TANK_DB_NAME:?name of the database for tank}
      TANK_DB_PASSWORD: ${AURA_TANK_DB_PASS:?password for tank DB}
      TANK_DB_USERNAME: ${AURA_TANK_DB_USER:?database user for tank DB}
      OIDC_CLIENT_ID: ${AURA_TANK_OIDC_CLIENT_ID:?oidc-client-id for tank}
      OIDC_CLIENT_SECRET: ${AURA_TANK_OIDC_CLIENT_SECRET:?oidc-client-secret for tank}
      AURA_ENGINE_SECRET: ${AURA_TANK_ENGINE_PASSWORD:?password for engine user}
      AURA_HOST: ${AURA_HOST_NAME:?host domain of this aura deployment}
      AURA_PROTO: ${AURA_HOST_PROTO:-https}
    volumes:
      # Uncomment as soon Tank provides an overwrite logic
      # @see https://gitlab.servus.at/aura/aura/-/issues/132#note_7497
      # - ./../services/:/etc/aura/:ro
      - ${AURA_AUDIO_STORE_SOURCE:-/opt/aura/audio/source/}:/srv/audio
      - ${AURA_AUDIO_STORE_IMPORT:-/opt/aura/audio/import/}:/var/audio/import
      - ${AURA_LOGS:-/opt/aura/logs}/tank:/srv/logs
    networks:
      - auranet
      - internal-tank
    healthcheck:
      test: [ "CMD", "nc", "-z", "-w", "3", "localhost", "8040" ]

  dashboard:
    container_name: dashboard
    image: autoradio/dashboard:${AURA_DASHBOARD_VERSION}
    networks:
      - auranet
    expose:
      - "80"
    environment:
      TZ: ${TIMEZONE}
      AURA_HOST: ${AURA_HOST_NAME:?host domain of this aura deployment}
      AURA_PROTO: ${AURA_HOST_PROTO:-https}
      DASHBOARD_OIDC_CLIENT_ID: ${AURA_DASHBOARD_OIDC_CLIENT_ID:?dashboard client id}
    healthcheck:
      test: [ "CMD", "nc", "-z", "-w", "3", "localhost", "80" ]

  dashboard-clock:
    container_name: dashboard-clock
    image: autoradio/dashboard-clock:${AURA_DASHBOARD_CLOCK_VERSION}
    networks:
      - auranet
    expose:
      - "5000"
    ports:
      - "${DASHBOARD_CLOCK_BIND_URL:-127.0.0.1:5001}:5000"
    environment:
      TZ: ${TIMEZONE}
      DASHBOARD_CLOCK_HOST: ${AURA_DASHBOARD_CLOCK_HOST:-${DASHBOARD_CLOCK_BIND_URL:-127.0.0.1:5001}}
      NAME: ${AURA_DASHBOARD_CLOCK_NAME:-Studio Clock}
      LOGO_URL: ${AURA_DASHBOARD_CLOCK_LOGO_URL:-https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-logo.png}
      LOGO_SIZE: ${AURA_DASHBOARD_CLOCK_LOGO_SIZE:-100px}
      ENGINE_URL: ${AURA_ENGINE_API_EXTERNAL_URL}
      UNKNOWN_TITLE_STRING: ${AURA_DASHBOARD_CLOCK_UNKNOWN_TITLE_STRING:-Unknown Title}
      NO_CURRENT_TIMESLOT_STRING: ${AURA_DASHBOARD_CLOCK_NO_CURRENT_TIMESLOT_STRING:-No show playing}
      NO_NEXT_TIMESLOT: ${AURA_DASHBOARD_CLOCK_NO_NEXT_TIMESLOT_STRING:-Nothing scheduled next}
      PLAY_OFFSET: ${AURA_DASHBOARD_CLOCK_PLAY_OFFSET:-3}
      CSS: ${AURA_DASHBOARD_CLOCK_CSS}
      FALLBACK_TEXT: ${AURA_DASHBOARD_CLOCK_FALLBACK_TEXT}

  icecast:
    image: moul/icecast:latest
    environment:
      TZ: ${TIMEZONE}
      ICECAST_SOURCE_PASSWORD: ${ICECAST_SOURCE_PASSWORD}
      ICECAST_ADMIN_PASSWORD: ${ICECAST_ADMIN_PASSWORD}
      ICECAST_PASSWORD: ${ICECAST_PASSWORD}
      ICECAST_RELAY_PASSWORD: ${ICECAST_RELAY_PASSWORD}
      ICECAST_HOSTNAME: ${AURA_HOST_NAME}
    # Reverse proxying the stream isn't (easily) possible at the moment, so we also forward port 8000.
    # The interface will be reachable via that port too, but by default it will be reachable under
    # AURA_HOST_NAME/icecast through the nginx of dashboard. Audio streams will access port 8000 directly.
    ports:
      - "0.0.0.0:8000:8000"
    networks:
      - auranet
    profiles:
      - icecast

  nginx:
    image: gitlab.servus.at:5050/aura/aura:nginx-unprivileged-certbot
    # Nginx runs with the uid 101, 2872 is the aura common group
    user: 101:2872
    networks:
      - auranet
    depends_on:
      steering:
        condition: service_healthy
      tank:
        condition: service_healthy
      dashboard:
        condition: service_healthy
    environment:
      TZ: ${TIMEZONE}
      AURA_HOST: ${AURA_HOST_NAME:?host domain of this aura deployment}
      AURA_PROTO: ${AURA_HOST_PROTO:-https}
      CERTBOT_EMAIL: ${AURA_HOST_CERTBOT_EMAIL:-admin@example.com}
      RUN_CERTBOT: ${AURA_HOST_CERTBOT_ENABLE:-true}
      INCLUDE_ICECAST: ${INCLUDE_ICECAST:-false}
      INCLUDE_CLOCK: ${INCLUDE_CLOCK:-false}
      ENGINE_API_URL: ${AURA_ENGINE_API_INTERNAL_URL:-engine-api:8008}
    volumes:
      - ../services/nginx/run.sh:/run.sh
      - ../services/nginx/nginx.prod.template:/etc/nginx/conf.d/nginx.template
      - ../services/nginx/icecast.conf:/etc/nginx/conf.d/locations/icecast.conf
      - ../services/nginx/clock.conf:/etc/nginx/conf.d/locations/clock.conf
      - ../services/nginx/proxy.conf:/etc/nginx/proxy.conf
      - letsencrypt_certs:/etc/letsencrypt
      - steering_static:/usr/share/nginx/html/steering/static
      - steering_media:/usr/share/nginx/html/site_media
      - ${AURA_LOGS}/nginx:/var/log/nginx
      - ${AURA_LOGS}/letsencrypt:/var/log/letsencrypt

    ports:
      - "0.0.0.0:80:80"
      - "0.0.0.0:443:443"

volumes:
  steering_db_data:
  steering_static:
  steering_media:
  tank_db_data:
  letsencrypt_certs:

networks:
  auranet:
    name: auranet
  internal-steering:
  internal-tank:
