# sample settings for steering

from .settings import *  # noqa

ADMINS = [("Ernesto", "ernesto@helsinki.at")]
MANAGERS = ADMINS

TIME_ZONE = "America/La_Paz"

