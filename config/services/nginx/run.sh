#!/bin/bash

# When we get killed, kill all our children
set -eu
trap "exit" INT TERM
trap "kill 0" EXIT
envsubst "`env | awk -F = '{printf " $$%s", $$1}'`" < /etc/nginx/conf.d/nginx.template > /etc/nginx/conf.d/default.conf

if [ "$INCLUDE_ICECAST" = "true" ]; then
  sed -Ei 's/^#([[:blank:]]*include \/etc\/nginx\/conf.d\/locations\/icecast.conf;)/\1/' /etc/nginx/conf.d/default.conf
else
  sed -i 's/^[[:blank:]]*include \/etc\/nginx\/conf.d\/locations\/icecast.conf;/#&/' /etc/nginx/conf.d/default.conf
fi

if [ "$INCLUDE_CLOCK" = "true" ]; then
  sed -Ei 's/^#([[:blank:]]*include \/etc\/nginx\/conf.d\/locations\/clock.conf;)/\1/' /etc/nginx/conf.d/default.conf
else
  sed -i 's/^[[:blank:]]*include \/etc\/nginx\/conf.d\/locations\/clock.conf;/#&/' /etc/nginx/conf.d/default.conf
fi

echo "generated nginx-config"

if [ "$RUN_CERTBOT" = "true" ]; then
  echo "Running Certbot"
  # check for undefined variables
  set -u
  # Run certbot which should get certificates and modify your nginx config
  certbot --nginx --domains ${AURA_HOST} --email ${CERTBOT_EMAIL} --agree-tos --redirect --reinstall --non-interactive --http-01-port=8080

  cat /etc/nginx/conf.d/default.conf
  echo "Running nginx with the above configuration."

  while [ true ]; do
    echo "Running certbot renew"
    certbot renew --non-interactive --http-01-port=8080
    nginx -s reload

    # Sleep for 1 week
    sleep 604810 &
    SLEEP_PID=$!

    # Wait on sleep so that when we get ctrl-c'ed it kills everything due to our trap
    wait "$SLEEP_PID"
  done
else
  echo "not requesting certificates"
  cat /etc/nginx/conf.d/default.conf
  /bin/sh /docker-entrypoint.sh nginx -g "daemon off;"
fi
