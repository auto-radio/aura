map $http_x_forwarded_proto $redirect_proto {
    default "$http_x_forwarded_proto";
    ''      "$scheme";
}

map $http_x_forwarded_host $redirect_host {
    default "$http_x_forwarded_host";
    ''      "$host";
}

map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
    listen 80;
    server_name ${AURA_HOST};
    access_log /var/log/nginx/access.log;
    access_log /dev/stdout;
    error_log /var/log/nginx/error.log;
    error_log /dev/stderr;

    resolver 127.0.0.11 ipv6=off;

    location / {
        proxy_pass http://dashboard:80/;

        include proxy.conf;
    }
    location /openid/ {
        proxy_pass http://steering:8000/openid/;
        proxy_redirect $redirect_proto://$redirect_host/oidc_callback.html /oidc_callback.html;
        proxy_redirect $redirect_proto://$redirect_host/oidc_callback_silentRenew.html /oidc_callback_silentRenew.html;
        proxy_redirect $redirect_proto://$redirect_host /;
        proxy_redirect $redirect_proto://$redirect_host/ /;
        proxy_redirect ~^/?(.*)$ $redirect_proto://$redirect_host/steering/$1;

        include proxy.conf;
    }
    location /steering/ {
        proxy_pass http://steering:8000/;
        proxy_redirect $redirect_proto://$redirect_host/ /;
        proxy_redirect ~^/?(.*)$ $redirect_proto://$redirect_host/steering/$1;

        sub_filter_types text/html application/json;

        sub_filter 'href="/' 'href="/steering/';
        sub_filter 'src="/' 'src="/steering/';
        sub_filter 'action="/' 'action="/steering/';
        sub_filter '/api/' '/steering/api/';

        sub_filter_once off;

        include proxy.conf;
    }
    location /steering/static {
        root /usr/share/nginx/html;
    }
    location /site_media {
        root /usr/share/nginx/html;
    }
    location /tank/ {
        proxy_buffering off;
        proxy_ignore_headers "X-Accel-Buffering";
        proxy_request_buffering off;

        client_max_body_size 1G;

        proxy_pass http://tank:8040/;
        proxy_redirect $redirect_proto://$redirect_host/ /;
        proxy_redirect ~^/?(.*)$ $redirect_proto://$redirect_host/tank/$1;

        sub_filter 'href="/' 'href="/tank/';
        sub_filter 'src="/' 'src="/tank/';
        sub_filter 'action="/' 'action="/tank/';
        sub_filter_once off;

        include proxy.conf;
    }
    
    location ~ ^/engine/api/v1/trackservice/?$ {
        set $target http://${ENGINE_API_URL}/api/v1/trackservice;
        proxy_pass $target;

        include proxy.conf;
    }
    location ~ ^/engine/api/v1/trackservice/current/?$ {
        set $target http://${ENGINE_API_URL}/api/v1/trackservice/current;
        proxy_pass $target;

        include proxy.conf;
    }
    include /etc/nginx/conf.d/locations/icecast.conf;
    include /etc/nginx/conf.d/locations/clock.conf;
    include /etc/nginx/conf.d/custom/*.conf;
}
