# Show Planning

To manage your show and its episode navigate to the tabs for _Show_ and _Media Management_.

### Show Management

`/* tbd */`

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-nav-show.png" />

### Media Management

`/* tbd */`

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-nav-media.png" />
