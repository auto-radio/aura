# Recurrence Rules

The recurrence rules are used to create the repetitions (timeslots) of a scheduled show within the
first and the last date of the schedule, between the start and the end time.

The recurrence rules define the *repetition rhythms* that a radio station supports and therefore
are different from radio station to radio station.

- [Radio Helsinki](https://helsinki.at), supports weekly, bi-weekly and four-weekly shows.
- [Radio Orange 94.0](https://o94.at), supports weekly, bi-weekly and monthly shows.

The following recurrence rules are supported by default if the `rrule.json` fixture is loaded.

| name                        | freq | interval | by_set_pos | count | by_weekdays | notes |
|-----------------------------|------|----------|------------|-------|-------------|-------|
| once                        | 0    |          |            | 1     |             | [^1]  | 
| daily                       | 3    |          |            |       |             |       |
| weekly                      | 2    | 1        |            |       |             | [^2]  |
| business days               | 2    | 1        |            |       | 0,1,2,3,4   |       |
| weekends                    | 2    | 1        |            |       | 5,6         |       |
| bi-weekly                   | 2    | 2        |            |       |             | [^2]  |
| four-weekly                 | 2    | 4        |            |       |             | [^2]  |
| monthly on the first        | 1    | 1        | 1          |       |             | [^3]  |
| monthly on the second       | 1    | 1        | 2          |       |             | [^4]  |
| monthly on the third        | 1    | 1        | 3          |       |             | [^5]  |
| monthly on the fourth       | 1    | 1        | 4          |       |             | [^6]  |
| monthly on the fifth        | 1    | 1        | 5          |       |             | [^7]  |
| monthly on the last         | 1    | 1        | -1         |       |             | [^8]  |
| bi-monthly on the first     | 1    | 2        | 1          |       |             | [^3]  |
| bi-monthly on the second    | 1    | 2        | 2          |       |             | [^4]  |
| bi-monthly on the third     | 1    | 2        | 3          |       |             | [^5]  |
| bi-monthly on the fourth    | 1    | 2        | 4          |       |             | [^6]  |
| bi-monthly on the fifth     | 1    | 2        | 5          |       |             | [^7]  |
| bi-monthly on the last      | 1    | 2        | -1         |       |             | [^8]  |
| three-monthly on the first  | 1    | 3        | 1          |       |             | [^3]  |
| three-monthly on the second | 1    | 3        | 2          |       |             | [^4]  |
| three-monthly on the third  | 1    | 3        | 3          |       |             | [^5]  |
| three-monthly on the fourth | 1    | 3        | 4          |       |             | [^6]  |
| three-monthly on the fifth  | 1    | 3        | 5          |       |             | [^7]  |
| three-monthly on the last   | 1    | 3        | -1         |       |             | [^8]  |
| four-monthly on the first   | 1    | 4        | 1          |       |             | [^3]  |
| four-monthly on the second  | 1    | 4        | 2          |       |             | [^4]  |
| four-monthly on the third   | 1    | 4        | 3          |       |             | [^5]  |
| four-monthly on the fourth  | 1    | 4        | 4          |       |             | [^6]  |
| four-monthly on the fifth   | 1    | 4        | 5          |       |             | [^7]  |
| four-monthly on the last    | 1    | 4        | -1         |       |             | [^8]  |

The actual IDs are not *hard-coded* in any way in `steering`, only the parameters of the recurrence
rules are used to create the repetitions.

The only requirement is that the recurrence rule ID in your API call, or the dashboard, matches the
recurrence rules store in the `steering` database.

If the frequency is either weekly or monthly, the weekday of the first date in the schedule
determines the weekday of the repetition.

[^1]: The parameter `freq=0` actually describes a yearly frequency, but together with `count=1`, it
achieves what we want: A single timeslot on the first date of the schedule.

[^2]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
weekly every Monday if `interval=1`, biweekly on Monday if `interval=2` or four-weekly on Monday if
`interval=4`.

[^3]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the first Monday if `interval=1`, bi-monthly on the first Monday if `interval=2`,
three-monthly on the first Monday if `interval=3`, and four-monthly on the first Monday if
`interval=4`.

[^4]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the second Monday if `interval=1`, bi-monthly on the second Monday if `interval=2`,
three-monthly on the second Monday if `interval=3`, and four-monthly on the second Monday if
`interval=4`.

[^5]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the third Monday if `interval=1`, bi-monthly on the third Monday if `interval=2`,
three-monthly on the third Monday if `interval=3`, and four-monthly on the third Monday if
`interval=4`.

[^6]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the fourth Monday if `interval=1`, bi-monthly on the fourth Monday if `interval=2`,
three-monthly on the fourth Monday if `interval=3`, and four-monthly on the fourth Monday if
`interval=4`.

[^7]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the fifth Monday if `interval=1`, bi-monthly on the fifth Monday if `interval=2`,
three-monthly on the fifth Monday if `interval=3`, and four-monthly on the fifth Monday if
`interval=4`.

[^8]: If the first date of the schedule is "2023-01-02" (a Monday), the resulting recurrence is
monthly on the last Monday if `interval=1`, bi-monthly on the last Monday if `interval=2`,
three-monthly on the last Monday if `interval=3`, and four-monthly on the last Monday if
`interval=4`.
