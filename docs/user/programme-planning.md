# Programme Planning

The _Programme Coordinators_ home is the calendar tab.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-nav-calendar.png" />

## Calendar & Schedule Management

Here you can get a quick overview what the current programme looks like.
This is also the place to create schedules and timeslots based on recurrence rules.

`/* tbd */`

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/screenshot-dashboard-calendar.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/screenshot-dashboard-calendar.png)

### Creating schedules and timeslots

To create a new timeslot, click the desired area in the calendar. The following dialog opens.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/calendar-schedule-create.png" />

The calendar is able to detect collisions with existing, conflicting timeslots.

To learn more about this behaviour, check out the chapter below.

### Updating schedules and timeslots

Click the timeslot to open its dialog.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/calendar-schedule-edit.png" />

### Deleting schedules and timeslots

Click the timeslot to open its dialog.

On the bottom you find various options to either

- Delete all timeslots and its schedule
- Delete all timeslots in the future
- Delete the current timeslot only.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/calendar-schedule-delete.png" />

## Timeslot Collision Detection

When using the Dashboard Calendar to create new schedules or timeslots, there is some logic in
place to avoid collisions of conflicting timeslots.

When a collision is detected, the user interface offers you options to solve the conflict.

### Single collision, fully overlapping

In this situation an existing and your newly created timeslot are fully overlapping.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-fully-overlapping.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-fully-overlapping.png" /></a>

### Single collision, partly overlapping

Here only some duration of the new timeslot is overlapping an existing one.

Here you have the option to truncate your timeslot or the existing one. Additionally, the
solutions offered in the previous scenarios are offered here too.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-partly-overlapping.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-partly-overlapping.png" /></a>

### Single collision, superset and subset

In that scenario, conflicting timeslots are offered to be split. Again, all possible solutions
from the previous scenarios are valid too.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-superset-subset.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/collision-single-superset-subset.png" /></a>
