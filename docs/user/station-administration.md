# Radio Station Administration

Programme Coordinators have some additional features for radio management at hand.

## Manage Shows

### Creating a new show

In order to create a new show, click _plus_ button in the show management tab.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/show-create-button.png" />

In the dialog enter your show details, select the _show type_ and _funding category_. Then click _OK_.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/show-create-dialog.png" />

If you are starting from scratch, you might not have any _show types_ and _funding categories_ defined yet.

Learn how to define categorization options in the _Manage Metadata_ chapter below.

## Manage Metadata

There are different kinds of metadata which can be used to attribute shows.

```{admonition} Steering Backend Settings
:class: tip
These settings currently can be only adjusted in the Steering backend located at `/steering/admin/`. In the future there will be a dedicated *Radio Station Settings* section in Dashboard.
```

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-metadata-groups.png" />

### Categories

Categories can be utilized to create sections within the radio programme.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-category-list.png" />

For example Radio ORANGE 94.0 uses categories to group their shows in form of *areas of interest* on their website.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-category-list-website-o94.png" />

### Funding Categories

Funding categories are used to mark shows as per funding-related and regulartory purposes.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-funding-category-list.png" />

### Languages

Languages can be assigned on show level.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-language-list.png" />

### Music Focus

Here you can define a list of music genres and assign them to shows.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-music-focus-list.png" />

### Topics

This a way to group shows under one theme or special programme (German: "Schwerpunktprogramm").

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-topic-list.png" />

### Types

Types can be used to define the format of the shows. For example you can mark shows as a *feature* or *talkshow*.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-type-list.png" />

The following image gives an idea how Radio Helsinki uses this metadata group on their website.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-type-list-website-helsinki.png" />


## Manage users and groups

In order to manage users, you have to login to the Steering administration interface.

You can reach that admin interface via `/steering/admin/`. If your AURA installation is available
under `aura.myradio.org` the URL to reach that interface is `aura.myradio.org/steering/admin/`.

Here you can log in with your usual credentials you are using for _Dashboard_ too.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-user-group-overview.png" />

The screenshot outlines the section relevant for managing users and groups.

### Listing all available users

To get a list of available users click "_Benutzer_".

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-user-list.png" />

From here you can click individual users for editing their details. It's also possible to search or
filter users by different attributes.

### Creating new users

To create a new users, click the button "_Benutzer hinzufügen".

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-user-add.png" />

```{admonition} LDAP creates users automatically
:class: tip
Please check back with your IT team, if you are having LDAP in place. When using LDAP there is no
need to create users manually.
```

There you need to enter a username and password, then click "_Sichern und weiter Bearbeiten_".

On the following page you can choose to either create one of these account types:

- **Mitarbeiter:** Click the checkbox _Mitarbeiter-Status_
- **Administrator:** Click the checkbox _Administrator-Status_

After that click "_Sichern_".

```{admonition} Granular permissions
:class: tip
There is also a box where you can assign granular permissions to each user account. At the current
stage many settings have no effect yet. A basic set of role and permission management features
[will be provided](https://gitlab.servus.at/aura/aura/-/issues/156) in the coming releases.
```

`/* tbd */`

### Deactivating and deleting users

The administration panel allows you to delete users by pressing the "Delete" button.

Alternatively, you can also de-select the "active" checkbox. This means the user data is still
available, wherever it's needed, but the user is not able to login anymore.

```{admonition} Always prefer deactivation over deletion
:class: tip
When in doubt which approach you should choose, always prefer to deactivate the user account.
Deleting a user destroys all the historic data and should only be chosen when you are aware
about the consequences.
```

## Listing user groups

Click "_Gruppen_" to get a list of available groups.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-group-list.png" />

Groups are usually used to provide different features and permissions per user role.

Typical roles are:
- Superuser or administrator: This group is always available and not listet in this view
- Programme Coordinator: Managers of the radio schedule
- Host: People who are producing radio shows

### Editing a user group

To edit the group click the its name in the list.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-group-edit.png" />

In that user interface you can change the group's name or edit the set of permissions for that group.

```{admonition} Granular permissions
:class: tip
At the current stage many permission settings have no effect yet. A basic set of role and permission
management features [will be provided](https://gitlab.servus.at/aura/aura/-/issues/156) in the coming
releases.
```

### Adding a user group

To add a group click the button `Gruppe hinzufügen`.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/admin/admin-group-add.png" />

Please note, that this should be planned with the IT administration, since group are usually carefully
planned and defined within the whole organisation.

## LDAP Authentication & Authorization

`/* tbd */`

## Generating reports

Most radios require the generation of annual reports for regulatory purposes.

```{admonition} Reporting feature in Dashboard
:class: tip
At the moment reports have to be compiled by some administrator. In the future Dashboard will
provide a handy [button to generate reports](https://gitlab.servus.at/aura/aura/-/issues/178).
```

### Schedule reporting

`/* tbd: Mention Steering API to query reports on the radio schedule */`

### Play-out reporting

To generate playlog reports you can use the [`playlog` endpoint](https://api.aura.radio/engine/#/internal/list_playlog)
of Engine API. These reports have to be manually generated and aggregated for now.

Learn how playlogs are structured in the API Schema for [`PlayLog`](https://api.aura.radio/engine/).


