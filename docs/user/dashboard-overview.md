# Dashboard Overview

The dashboard is a simple browser application, allowing:

- _Hosts_ to upload and manage their shows.
- _Programme Coordinators_ to manage the radio station and its programme.

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/screenshot-dashboard-overview.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/screenshot-dashboard-overview.png)

## Login

Click the _Sign In_ button to open your personal dashboard.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-sign-in.png" />

Use the credentials provided by your radio station.

## Logout

To sign out click your profile name in the right top. Then click _Sign Out_.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-sign-out.png" />

## Navigation Bar

Here you can open tabs to manage:

- Shows
- Media
- Programme Calendar

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-nav-show.png" />

## User Profile

`/* tbd */`

## User Settings

`/* tbd */`

## Switch Language

To change the language click the language picker on the right top. Then choose your preferred language.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-language.png" />

## Version

When reporting bugs you will be asked about the Dashboard version your are using.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/screenshots/dashboard-version.png" />

You find the version in the footer of the application.
