# AURA - Automated Radio

AURA is a software suite for community radio stations. All code is Open Source and licensed under [AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

```{admonition} Alpha Status
:class: tip

We are currently in alpha status, so do not use AURA in production yet. If you are an early-stage user or developer, we would be happy about your [contributions](contribute/contributions.md).
```

Get an overview on the current release at the [Release Notes](release-notes.md) page.


```{toctree}
---
caption: Guides
maxdepth: 1
glob: True
---

user/index
administration/index
developer/index
```

```{toctree}
---
caption: Contribute
maxdepth: 1
glob: True
---
release-notes
contribute/bug-reports
contribute/contributions
contribute/code_of_conduct
about
```

```{toctree}
---
caption: Communicate
maxdepth: 1
glob: True
---

communicate/matrix
communicate/mailinglist
```

## Partners

<a href="https://o94.at/"><img src="https://o94.at/themes/custom/radio_orange/logo2.png" width="500" /></a>
<br/><br/>
<a href="https://helsinki.at/"><img src="https://helsinki.at/wp-content/uploads/logo.png" width="300" /></a>
<br/><br/>
<a href="https://www.fro.at/"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/reserved-copyright/logo-radio-fro.jpg" width="150" /></a>
<br/><br/>
<a href="https://wueste-welle.de/"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/reserved-copyright/logo-wueste-welle.png" width="330" /></a>
<br/><br/>
<a href="https://www.frf.at/"><img src="https://www.frf.at/wp-content/uploads/cropped-FRFlogofont_einzeil-700x95-1.jpg" width="400" /></a>
<br/><br/>
<a href="https://freefm.de/"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/reserved-copyright/logo-free-fm.png" width="150" /></a>
<br/><br/>
<a href="https://freirad.at"><img src="https://neu.freirad.at/wp-content/uploads/2021/09/FREIRAD-Logo-4c-pos-klein.jpg" width="250" /></a>
