# Deployment Bundles

Same as a car has fixed and moving parts, AURA likewise has parts which are fundamental and parts which are optional.

In order to keep administrators life simple, we have defined component spaces reflecting to most common deployments:

- AURA Web: Contains all the web facing services, including digital asset management
- AURA Playout: Contains all things required for scheduling & broadcasting
- AURA Record: Record audio and periodical sync to archive

These spaces are realized as of [Docker Compose](https://docs.docker.com/compose/) bundles.

## AURA Web

| Service         | Required | Description                                                                   |
| --------------- | -------- | ----------------------------------------------------------------------------- |
| steering        | ✅       | Single-source of truth, holding all radio, show, host<br/>and scheduling data |
| tank            | ✅       | Upload, download, normalization and media asset management                    |
| dashboard       | ✅       | Backend user interface                                                        |
| dashboard-clock |          | Studio clock                                                                  |
| play            |          | A library for building frontends based on web components                      |

The _required_ services are needed for minimal functionality.

## AURA Playout

| Service         | Required | Description                              |
| --------------- | -------- | ---------------------------------------- |
| engine          | ✅       | Control and scheduling for the play-out  |
| engine-core     | ✅       | Play-out Engine                          |
| engine-api      |          | API for playlogs and track service       |
| engine-recorder |          | Record audio blocks, including archiving |

The _required_ services are needed for minimal functionality.

## AURA Recorder

| Service         | Required | Description                                 |
| --------------- | -------- | ------------------------------------------- |
| engine-recorder | ✅       | Record audio blocks, including archive sync |

The _required_ services are needed for minimal functionality.
