# Overview & Features

The following diagram outlines the main elements of AURA.

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-simplified-overview.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-simplified-overview.png)

## The naming scheme

Automated Radio, AutoRadio, AuRa or AURA, follows an "_Automobile-naming-scheme_". All the components are named after some crucial parts of a car:

- **Steering**: This is the single source of thruth, holding all the radio data, hence steering some radio's broadcast.
- **Tank**: Just spleen without steam. That's where your shows, tunes and recordings are managed. Fuelling your broadcast with materials.
- **Dashboard**: Observe and control what's happening. That's the backend user interface of your radio with individual views for hosts and programme coordinators.
- **Engine**: The playout server allowing you to broadcast via FM and web audio streams. Includes silence detection and optional recording options.
- **Play**: Every car has some fancy _Auto Radio Player_. And every radio station has the need for some modern frontend experience. _Play_ is a library of web components. Making web development a breeze. Just like _playing with blocks of Lego_.

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-steering.png" width="128" style="float:left" alt="AURA Steering" />
<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-dashboard.png" width="128" style="float:left" alt="AURA Dashboard" />
<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-tank.png" width="128" style="float:left" alt="AURA Tank" />
<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="128" style="float:left" alt="AURA Engine" />
<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-play.png" width="128" style="float:left" alt="AURA Play" />

## Features

The aforementioned naming scheme is used as a basis for name-spacing individual services.

Here you get an overview of the features of the provided services.

Find details on additional functionality on the [bundle documentation](documentation/index) pages or in the configuration files of individual services.

### Steering

`/* to be defined */`

### Dashboard

`/* to be defined */`

### Dashboard Clock

Web Application providing a Studio Clock.

`/* more to be defined */`

### Tank

`/* to be defined */`

### Tank Cut & Glue

`/* to be defined */`

### Engine

Scheduling and control for the playout.

- **Scheduler:** Automatically broadcast your radio programme (see [AURA Dashboard](https://gitlab.servus.at/aura/dashboard) for a user interface to do scheduling)
- **Autonomous playout:** Schedule information is pulled from Steering into a local cache. This way the playout keeps working, even when the network connectivity might be lost.
- **Versatile Playlists:** Playlists can contain different content types, such as audio files, audio streams and line in channels of you audio interface.
- **Default Playlists:** Define playlists on show and schedule level, in addition to any timeslot specific playlists. This way you always have an playlist assigned, when some host forgets about scheduling a specific programme.
- **Heartbeat Monitoring:** Frequently send pulse to a monitoring server, ensuring your schedule and playout server is up and running.

`/* more to be defined */`

### Engine Core

The playout server based on Liquidsoap.

- **Multi-channel input:** Play audio from various sources including queues, streams and line-in from the audio interface
- **Multi-channel output:** Output to line out or audio streams
- **Icecast connectivity:** Stream to an Icecast Server, provide different encoding formats and bitrates
- **Auto DJ triggered by Silence Detector:** Play fallback audio triggered by a silence detector to avoid _Dead Air_. Play randomized music from a folder or M3U playlist.
- **Metadata handling:** Send information on playlogs via REST calls to external services like Engine API.
- **ReplayGain:** Normalization done using passed _ReplayGain_ meta data.

### Engine API

An OpenAPI 3 service to store and retrieve Engine data.

`/* more to be defined */`

### Engine Recorder

A simple but powerful recorder.

- **Bulk Recorder**: Record blocks of audio for archiving, audit-logging or further processing
- **Sync to archive**: Periodically synchronize recordings to a defined destination
