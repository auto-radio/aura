# AURA Web

## Deploy AURA Web

All required files can be found under `/opt/aura/config/aura-web`. For the described commands change to that directory.

Copy the `sample.env` to `.env` and set all values as needed.

### Update the configuration file

Update at least following environment variables in the `.env` file:

- `AURA_AUDIO_STORE_SOURCE`: The location where Tank is storing audio sources. It defaults to `aura/audio/source`.
- `AURA_TANK_ENGINE_PASSWORD`: The password should match the one you'll be configuring in AURA Playout. Avoid using the default one.
- `AURA_HOST_NAME`: Set the host or domain name where the service are used from. See below why to avoid `localhost`.
- `AURA_HOST_PROTO`: Defaults to `https`. Set to `http` if you are running locally.
- `AURA_HOST_CERTBOT_ENABLE`: Enable in production in order to get TLS certificates automatically.
- `AURA_HOST_CERTBOT_EMAIL`: Set a valid email for certbot.

```{admonition} Defining the host for production setups
:class: tip
You can use a public domain name or any locally defined hostname. For example by configuring
`aura.local` in `/etc/hosts`. Keep in mind to use a LAN IP though. Using `localhost` or `127.0.0.1`
for the production setup is not supported. Here you'd need to make adjustments on your own or use
the dev-setup.
```

```{admonition} Changing the host
:class: alert
Any update of `AURA_HOST_NAME` in your `.env` file is not reflected in the actual configuration.
In such cases you either need to manually update the relevant OIDC client database tables, or you
simple create new OIDC client IDs in the configuration. After that you can delete any old OIDC
clients via the Steering admin interface `$AURA_HOST_NAME/steering/admin/oidc_provider/`.
```

In case you have deployed AURA Playout to a different instance, also set the value
for `AURA_ENGINE_API_INTERNAL_URL`.

### Initialize the steering database container:

To initialize the database containers for steering, you can call

```bash
$ docker compose run --rm steering initialize
```

This step will

- Create the database by running the migrations,
- Load the recurrence the rules from `fixtures/program`,
- Create the RSA Key
- Create the OpenID Connect clients for `dashboard` and `tank`,
- Create a super user.

### Start the services with Docker Compose:

```bash
$ docker compose up -d
```

We use named volumes for the data-volumes. That way all data will live in your docker-directory.

This deployment will make _AURA Web_ reachable in the following way:

- **Dashboard** is reachable directly under the domain given as `$AURA_HOST_NAME`
- **Steering** is reachable under `/steering`
- **Tank** is reachable under `/tank`
- **Track Service** endpoints track service will be reachable under `/engine/api/v1/trackservice`, if `engine-api` is running. Check the [Engine API specification](https://api.aura.radio/engine/) for details.
- **Icecast**: If you enabled the optional Icecast server for the reverse proxy it will be reachable under `/icecast`. If you only enabled it, it will be reachable under port `8000` of the machine running _AURA Web_.
- **Dashboard Clock** will be reachable from the configured network under the configured port. If you enabled it for the reverse-proxy, it will be reachable under `/clock`.

### Applying radio station settings with fixtures

The radio station settings are organized in so-called *fixtures*.

You can either load a set of sample data or define your own production settings.

#### Load sample fixtures

In case you want to load the sample fixtures, you can call

```bash
$ docker compose run --rm steering loaddata.sample
```

This step will load the fixtures from the `fixtures/sample` directory.

#### Load custom fixtures

In case you want to load custom fixtures, you can add them to the
`fixtures/custom` directory, and call

```bash
$ docker compose run --rm steering loaddata.custom
```

This step will load the fixtures from the `fixtures/custom` directory.

### Production settings

If you need to extend the settings for production beyond what is possible with
environment variables, you will need to create a settings module and start the
services with the production settings.

#### Settings module

Create `production_settings.py` inside the `steering` directory:

```python
# steering/production_settings.py

# import the base settings
from steering.settings import *  # noqa

# Import classes, functions, etc.
from django_auth_ldap.config import GroupOfNamesType
# and use them to extend or overwrite settings
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType(name_attr="cn")
```

Refer to the
[Django](https://docs.djangoproject.com/en/4.2/ref/settings/#settings),
[Django LDAP](https://django-auth-ldap.readthedocs.io/en/latest/reference.htmlh#settings),
[Django REST Framework](https://www.django-rest-framework.org/api-guide/settings/#api-reference),
or [DRF Spectacular](https://drf-spectacular.readthedocs.io/en/latest/settings.html#settings)
documentation for the settings available and their default values.

#### Start the services with the production settings

To start with the production settings, specify **both** `docker-compose.yml`
and `steering-production.yml` while starting the services.

```bash
$ docker compose -f docker-compose.yml -f steering-production.yml up -d
```

### Check the logs

Now everything should be up and running. You can check the logs with

```bash
$ docker compose logs -f --tail=100 <service-name>
```

See [Maintenance and updates](https://docs.aura.radio/en/latest/administration/maintain-and-update.html) for more Information.

## NGINX and Reverse Proxy

In the _AURA Web_ setup all services are made reachable behind a reverse Proxy. For this NGINX is
used. Here we also offer support for using SSL via Let's Encrypt.

Be aware that the docker-compose by default opens ports `80` and `443`. If you want to run
_AURA Web_ behind a reverse proxy on a different machine, you can just use port 80 of this
machine. At the moment running another reverse proxy on the same machine isn't supported with
this setup, since there's no possibility to close ports with an override.

If you wish to not use the NGINX whatsoever, you can override the docker-compose with the
following content:

```yaml
services:
  nginx:
    deploy:
      replicas: 0
```

This disables the internal NGINX. Keep in mind the nginx does a lot of heavy lifting to handle
all redirections and URL rewrites correctly, which you will then need to handle manually.

If you wish to expand the nginx-config you can put further configs into the `custom`-folder.
Keep in mind the configs need to end on `.conf`.

