# Deployment Scenarios

AURA comes with two Docker Compose bundles:

- **AURA Web**: Combines all essential containers required for the web-facing elements.
- **AURA Playout**: Everything related to scheduling, play-out and (optionally) recording.
- **AURA Record**: A standalone recorder and archive synchronisation.

There are also additional, individual Docker containers available to further extend the provided features.

The following network diagrams outline ideas on deployment scenarios.

## Single Instance

This is the most simple scenario for setting things up, as all services reside on the same machine. The downsite is, that any hardware issues or the need for restarting the server has an immediate effect on the full array of services.

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--single.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--single.png)

## Advanced

Here we utilize at least two machines for services: One for all web-facing applications (AURA Web), the other related to scheduling and play-out (AURA Playout). This is the most verstatile approach between simplicity and maintainability, since your play-out will still be functional, while shutting down the web server for updates.

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--advanced.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--advanced.png)

## High Availability

That's the most sophisticated approach where your radio should never ever go down. But it's also the most tricky one, which needs careful testing and more valves to maintain.

```{admonition} Future scenario
:class: tip

Please note that this is a future scenario. Not all required service are available yet.
```

[<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--ha.png" />](https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-network--ha.png)
