# AURA Playout

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="128" alt="AURA Engine" align="right" />

## Deploy AURA Playout

All required files can be found under `config/aura-playout`. For the described commands change
to that directory.

Then copy the `sample.env` to `.env` and set all values as needed.

### Update the configuration file

Update at least following environment variables in the `.env` file:

- `AURA_TANK_ENGINE_PASSWORD`: The password should match the one configured in AURA Web. Avoid using the default one.
- `AURA_AUDIO_STORE_SOURCE`: The location where Tank is storing audio sources. It defaults to `audio/source`. It should match the one set in AURA Web.
- `AURA_AUDIO_STORE_PLAYLIST`: The location where M3U playlists are provided. This is optional and defaults to `audio/playlist`.
- `AURA_AUDIO_STORE_FALLBACK`: The location where fallback audio is retrieved from. Such audio is played when nothing is scheduled. It defaults to `audio/fallback`.

Now populate the `AURA_AUDIO_STORE_FALLBACK` location with some audio files.

### Configure the audio interface

_AURA Playout_ requires you to have a ALSA compatible audio interface.

```{admonition} Disable PulseAudio server
:class: tip
PulseAudio introduces huge delays when controlling the play-out. In order to experience more
exact timings in scheduling ensure PulseAudio is disabled.
```

Now create the file `config/aura-playout/asound.conf` for the ALSA configuration. The configuration
should hold custom PCM device named `pcm.aura_engine`.

There is a sample ALSA configuration `sample.asound.conf` in the same directory. For more information about the ALSA configuration visit the [official documentation](https://alsa.opensrc.org/Asoundrc).

```{admonition} Your ALSA Device
:class: tip
To find the name and even more information about your audio device you can use `aplay -L` and  aplay -D <_audio_device> --dump-hw-params`. See our [FAQ](https://docs.aura.radio/en/latest/administration/aura-playout.html#faq) for more information.
```

### Start the services with Docker Compose:

```bash
docker compose up -d
```

After successful start-up, you should hear some music playing already.

```{admonition} Latency issues with live analog audio
:class: tip

If you are facing latency issues with Engine Core while playing live from the analog audio source, we
suggest you try to deploy Engine Core natively. To do so you'll need to comment out the
`COMPOSE_PROFILES` variable in you `.env` file. Then, follow the documentation for a
[native installation of Engine Core](https://gitlab.servus.at/aura/engine-core#prerequisites).
We hope to solve the Docker latency issue in one of the coming releases.
```

## Playout channel routing

Playout channels are routed this way:

`/* FIXME: render Mermaid diagram */`

```mermaid
graph TD
    iq0[Queue A] -->|in_queue_0| mix
    iq1[Queue B] -->|in_queue_1| mix
    is0[Stream A] -->|in_stream_0| mix
    is1[Stream B] -->|in_stream_1| mix
    il0[Line In 1-5] -->|in_line_0..4| mix
    ff[Fallback Folder] -->|fallback_folder| which_fallback
    fpls[Fallback Playlist] -->|fallback_playlist| which_fallback
    mix["  Mixer  "] --> silence_detector
    which_fallback{or} -->| | silence_detector{Silence Detector}
    silence_detector -->| | output[Output]
    output --> |output.alsa| C[fa:fa-play Audio Interface]
    output --> |output.icecast| D[fa:fa-play Icecast]
```

## Configuring playout

### Configure the audio interface

By default only audio output is enabled using the systems default ALSA device.

```{admonition} Ensure PulseAudio server is disabled
:class: warning

You get the most glitch-free experience when using [ALSA](https://www.alsa-project.org/) devices directly. To avoid any play-out malfunctions ensure that no PulseAudio server is running.

To check if PulseAudio is started, run `pactl list`. If this command returns an error, PulseAudio is deactivated.
```

If you want to choose a different device, edit the configuration file and set a value for `output_device_0`.

It's also possible to set ALSA hardware device IDs like `hw:0,0`. Check the FAQ below on how to retrieve available audio device IDs.

```{admonition} Recommended audio device configuration
:class: tip

For better I/O performance it's recommended to create an ALSA PCM device named `pcm.aura_engine`. You can use the sample configuration
[`sample.asoundrc`](https://gitlab.servus.at/aura/engine-core/-/blob/main/config/sample.asoundrc) as a basis for creating such device. Consult the ALSA documentation on details. After creating such device verify its properly assigned in the configuration file as `output_device_0="pcm.aura_engine"`.
```

### Optional deploy playout with Aura-Recorder

By default the Docker-Compose will not deploy the the Aura-Recorder.
If you want to deploy the Aura-Recorder on the same Host start the compose service with 
```bash
docker compose --profile engine-recorder-enabled up -d
```

## Configure the audio source locations

Engine Core is requires different audio sources in order to perform the playout.

### Configure the location for fallback music

By default fallback audio is retrieved from the `fallback` folder. A local folder for any
emergency playback, also called _Station Fallback_.

```ini
audio_fallback_folder="audio/fallback/"
```

All audio files inside are played in a randomized order, in situations where nothing is scheduled.
The folder is being watched for changes. So you can add/remove audio on the fly.

This fallback feature is enabled by default, but can be turned off in via the configuration.

Instead of the fallback folder you can use a playlist in the `playlist` folder for fallback scenarios.
Its default file name is `station-fallback-playlist.m3u` and located in:

```ini
audio_playlist_folder="audio/playlist"
```

Also this playlist is being watched for changes. You'll need to set the configuration option
`fallback_type="playlist"` to enable this instead of the fallback folder.

### Configure the audio source folder

This is the location for actually scheduled audio files. They are provided by Tank.

```ini
audio_source_folder="audio/source"
```

If you are running all AURA services on a single instance you should be fine with just creating a
symbolic link to the relevant Tank folder (`ln -s $TANK_STORE_PATH $PLAYOUT_AUDIO_SOURCE`). But in some
[distributed and redundant production scenario](https://docs.aura.radio/en/latest/administration/deployment-scenarios.html)
you might think about more advanced options on how to sync your audio files between machines.

## Features and how they work

### Scheduler

Engine provide a scheduling functionality by polling external API endpoints frequently. Those API endpoints are provided by [Steering](https://gitlab.servus.at/aura/steering) to retrieve schedule information and [Tank](https://gitlab.servus.at/aura/tank) to retrieve playlist information. To define your schedule you'll also need [AURA Dashboard](https://gitlab.servus.at/aura/dashboard) which is an elegent web user interface to manage your shows, playlists and schedules.

Ideally any audio is scheduled some time before the actual, planned playout to avoid timing issues with buffering and preloading. Nonetheless, playlists can also be scheduled after a given calendar timeslot has started already. In such case the playout starts as soon it is preloaded.

If for some reason the playout is corrupted, stopped or too silent to make any sense, then this <u>triggers a fallback using the silence detector</u> (see chapter below).

> Note: If you delete any existing timeslot in Dashboard/Steering this is only reflected in Engine until the start of the scheduling window. The scheduling window is defined by the start of the timeslot minus a configured offset in seconds (compare your Engine configuration).

### Versatile playlists

It is possible to schedules playlists with music or pre-recorded shows stored on the **file system**, via external **streams** or live from an **line input** in the studio. All types of sources can be mixed in a single playlist.

The switching between types of audio source is handled automatically, with configured fadings applied.

> Note: Any live sources or streams not specifying a length property, are automatically expanded to the left duration of the timeslot.

### Default playlists

While a timeslot can have a specific playlist assigned, it is also possible to define default playlists
for schedules and shows:

- **Default Schedule Playlist**: This playlist is defined on the level of some recurrence rules (_Schedule_).
  In case the timeslot doesn't have any specific playlist assigned, this playlist is broadcasted.

- **Default Show Playlist**: This playlist can be assigned to some show. If neither the specific timeslot
  playlist nor the default schedule playlist is specified the _default show playlist_ is broadcasted.

If none of these playlists have been specified the _Auto DJ_ feature of _Engine Core_ takes over (optional).

### Heartbeat Monitoring

Instead of checking all status properties, the Heartbeat only validates the vital ones required to run the engine. If all of those are valid, a network socket request is sent to a defined server. This heartbeat is sent continuously based on the configured `heartbeat_frequency`. The service receiving this heartbeat ticks can decide what to do with that information. One scenario could be switching to another Engine instance or any other custom failover scenario. Under `engine/contrib/heartbeat-monitor` you'll find some sample application digesting these heartbeat signals.

## FAQ

### I am using the `default` audio device. How can I set another default device?

You can check the systems default audio hardware by executing `aplay -L` on the command line.

You can set the default device in `/etc/asound.conf` or `~/asoundrc`.

### How can I retrieve available ALSA audio devices

- To see only the physically available sound cards: `cat /proc/asound/cards`
- To see sound cards with all additional devices (e.g. HDMI): `aplay -l`
- To see devices configured by ALSA with additional plugins: `aplay -L`
- The default devices that should be used: `aplay -L | grep default`

### I have configured an audio device but still hear no sound (native installation)

To test if you device is able to output audio at all, independently from Engine Core, try executing `speaker-test`. Also checkout out the `-D` argument to test specific devices. If you system doesn't provide `speaker-test` you have to install or use your preferred way of testing also audio.

### I have configured an audio device but still hear no sound (Docker installation)

If you are running Engine Core using Docker, run the aforementioned `speaker-test` from within your docker container by perform following:

1. Bash into the container using `docker exec -it aura-engine-core bash`
2. Now run `speaker-test`. It that's working, you now know that your audio device is at least available from within Docker and you'll need to further check your Liquidsoap device configuration.
3. Next you can run `liquidsoap tests/test_alsa_default.liq`. This is a basic script which tries to play the supplied MP3 using the default ALSA device.

### I'm getting `clock.wallclock_alsa:2` Error when starting output lineout: Failure("Error while setting open_pcm: No such file or directory")!\*\*

Assure you have set the correct device ID. To do so read the paragraph above. Review the audio interface configuration settings and verify if the default settings `input_device_0` and `output_device_0` are valid device IDs.

In case your are _not_ running Engine Core within Docker, also check if your executing user (è.g. `engineuser`) belongs to the group `audio`.

### How to solve 'Error when starting output output_lineout_0: Failure("Error while setting open_pcm: Device or resource busy")!'?

You probably have set a wrong or occupied device ID. The device could be reserved by another software using the ALSA sound system. Or you might be accessing a device using ALSA which is already assigned to the Pulse Audio sound system. Here it could help to [remove the device from PulseAudio](https://jamielinux.com/blog/tell-pulseaudio-to-ignore-a-usb-device-using-udev/) before accessing it.

### How to avoid stutter, hangs, artifacts or in general glitchy sound?

This can have various reasons, but first of all it's good to check the `engine-core.log` file. Also check your CPU usage. Lastly review the settings of your audio device.

**Incorrect ALSA buffer settings**: If the ALSA settings provided by your system are not working cleanly the Engine Core settings provide to option to override parameters such as `alsa_buffer`. The correct settings are individual to the used soundcard but in general this is a tricky topic and deeper ALSA knowledge is very helpful.

**These problems occur while having Icecast streaming enabled**: Try to reduce the quality of the stream, especially when you are experiencing hangs on the stream. Check your Icecast connection. Is it up and running? Maybe there is some authentication issue or an [Icecast limitation for max clients](ttps://github.com/savonet/liquidsoap/issues/524).

**The hardware is hitting its limits**: Also check the relevant logs and the system utilization. Are there other processes using up the machines resources? You might even be hitting the performance limit of your hardware. Maybe using a realtime linux kernel could help too.
