# Deployment Preparations

## Requirements

- [Docker Engine 23.0.0 or later](https://docs.docker.com/engine/install/)
- [Docker Compose 2.15.1 or later](https://docs.docker.com/compose/install/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/)
- [make](https://www.gnu.org/software/make/)

## Setting up the home and aura user

Clone the `aura` repository to the server(s) you want to deploy _AURA Web_ or _AURA Playout_ at.

We recommend using `/opt/aura` as your home for the AURA installation.

```bash
sudo git clone https://gitlab.servus.at/aura/aura.git /opt/aura
```

Next, move to that directory and create the `aura` user. This user is used for running all services.

```bash
make aura-user.add
```

The directory under `/opt/aura` is now owned by `aura:aura`.

```{admonition} User permissions
:class: tip
All resources will be owned by the user `aura`, hence we recommend performing all subsequent commands
as this user or to add the current user to the `aura` group.
```

## Selecting the release

First check for available releases.

```
$ git tag
1.0.0-alpha1
1.0.0-alpha2
1.0.0-alpha3
```

This command returns a list of all tags corresponding to the available releases.
Alternatively you can take a look at [releases.aura.radio](https://releases.aura.radio).

To learn about changes between releases, consult the [Release Notes](../release-notes.md).

Then switch to the release you'd like to use.

```bash
git checkout tags/<release-version>
```

Replace `<release_version>` with one of the versions listed above, like `1.0.0-alpha1`.

```bash
git checkout tags/1.0.0-alpha1
```

```{admonition} Latest, unreleased state on the main branch
:class: tip
In case you want deploy the current development state, just take the latest commit on the `main`
branch. That's the state which is checked out by default. While we try to keep the `main` branch
as stable as possible, we cannot guarantee it being functional at all times. So please use it at
your own risk.
```

## Setting up the Audio Store

The _Audio Store_ is a folder which is utilized by _Tank_ and _Engine_ to exchange audio files.

Assuming both, _Engine_ and _Tank_ are hosted on different machines, audio folders must by shared
using some network share.

In case you are hosting _Engine_ and _Tank_ on the same machine, you can skip this step. Just
think about pointing the settings the relevant audio directories, or create a symlink to do
so behind the curtains.

By default the audio store is located in `/opt/aura/audio`. There are following subdirectories
expected:

- `source`: Holding all audio files from the media asset repository. Written by _Tank_ and read by _Engine_.
- `fallback`: Populate this folder with audio files to be played randomly, in cases where nothing is scheduled.
- `playlist`: Used for M3U audio playlists.
- `recordings`: The recorder stores its recorded blocks here.
- `import`: To import audio files into Tank via the filesystem, place them here.

### Share Type

Then, there's the question how the share is managed. Feasible options include:

- [NFS](https://en.wikipedia.org/wiki/Network_File_System)
- [SSHFS](https://en.wikipedia.org/wiki/SSHFS)
- [Gluster](https://www.gluster.org/)

Please evaluate for yourself what the most failsafe solution is. The next chapter outlines pro
and cons of different scenarios.

### Share Location

You have following options where your share can be located:

1. **Engine and all other AURA components (Tank, Dashboard, Steering) are running on the same instance.**
   This is the most simple solution, as Engine and Tank can share the same directory locally. But
   this scenario requires some more sophisticated tuning of the system resources to avoid e.g.
   some overload of multiple Uploads in Tank may affect the performance of engine. You can
   eliminate this risk by setting CPU and memory limits for Steering, Dashboard and Tank using
   Docker or `systemd-cgroups`. A disadvantage here is the case of maintenance of system reboot.
   This would mean that all components are offline at once.

2. **Physical directory where the Engine lives, mounted to Tank**. This may cause an issue with
   the mount, when the network connection to Engine is unavailable or the instance is rebooting.

3. **Physical directory where the Tank lives, mounted to Engine.** This may cause an issue with
   the mount, when the network connection to Tank is unavailable or the instance is rebooting.

4. **Central Data Store or _Storage Box_** which is mountet to Engine and Tank. In this case a
   downtime of the store make both, Engine and Tank dysfunctional.

5. **Replicated storage solution using [Gluster](https://www.gluster.org/), both Engine and Tank have their virtual audio directory mounted.**
   That's the ideal approach, because if any of the instances is down, the other has all the data
   available.

In any case, you should think about some backup solution involving this directory.

## Advanced Configuration

In the following steps, you will be advised to copy the provided `sample.env` file to `.env`. In
most common setup scenarios all configuration done in such `.env` file is sufficient.

For some more advanced setups or debug purposes, there are also sample configuration files for each
service under `/opt/aura/config/services/sample-config` available. To overwrite any service
configuration, simply copy its configuration file to the parent `services` folder.

Only use these overrides if you are an advanced user, or are advised to do so.

## Deployment

AURA can be deployed using Docker and Docker Compose, allowing custom-tailored orchestration.

In general we recommend the Docker Compose variant.