# AURA Recorder

<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="128" alt="AURA Engine" align="right" />

## Deploy AURA Recorder

All required files can be found under `config/aura-recorder`. For the described commands change to that directory. Make sure to follow the [preparation steps](https://docs.aura.radio/en/latest/administration/deployment-preparation.html) to add the user: `aura` first.

Then copy the `sample.env` to `.env` and set all values as needed.

```bash
su aura -c "cp sample.env .env"
```

### Update the configuration file

Update at least following environment variables in the `.env` file:

- `AURA_RECORDER_AUDIO_DEVICE`: The audio device from which the audio will be recorded. It is also possible to set a stream.
- `AURA_AUDIO_STORE`: The location where the recorder saves all recordings.

Make sure that the user `aura` can write to `AURA_AUDIO_STORE`. 

```bash
source .env
sudo mkdir -p "${AURA_AUDIO_STORE}/recordings/block"
sudo chown -R aura:aura "${AURA_AUDIO_STORE}"
```

```{admonition} Using a ALSA audio device
:class: warning

Currently AURA Recorder only supports ALSA devices or audio streams. When using a audio interface, make sure to provide the sample format in the [override config](https://gitlab.servus.at/aura/aura/-/blob/main/config/services/engine-recorder.yaml). For future releases we plan to read the sample rate directly from the audio device.
```

### Start the services with Docker Compose:

```bash
docker compose up -d
```

After successful start-up, you should see the `AURA_AUDIO_STORE` get populated. 

