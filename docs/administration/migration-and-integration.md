## Migration & Integration Plan

Before getting started you need to carefully plan how you want to fit AURA into your
infrastructure.

Check out the provided [Deployment Scenarios](deployment-scenarios.md) on ideas for
integration AURA into your ecosystem.

### Individual test & production instances

To keep your radio infrastructure up-to-date, AURA is meant to provide frequent releases.

In order to keep this process free of glitches, it's good to gain experience in maintaining
AURA on a test environment.

But also after moving AURA to production it is highly recommended to have a staging- or
test-instance, where new releases are deployed before actually updating the production servers.

### Migration Plan

Rome wasn't build in a day. And a radio software ecosystem isn't migrated in one day either.

Make a plan with different stages. Decide which parts you want to migrate at a certain stage.

Possible stages are:

1. Migrate and continuously synchronize scheduling and show data
2. Integrate your website and other frontends with AURA
3. Test run the actual play-out
4. Make the switch to the new play-out system

### Migration Tools

Several radios are building tools and scripts for migration. Besides offering Best Practices,
we can also share some code for a sample migration service, if that's what you need.

Let's talk in [Matrix](https://matrix.aura.radio).