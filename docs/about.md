```bash
[meta]: Hello, welcome, this is meta!
[you]: Oh, hi meta, what are you meta about?
[meta]: I am the place where all the work behind the 'real' work is happening.
[meta]: I collect stuff about the AuRa project and greet new visitors.
[you]: Ah, kool. Well... I am a new visitor, what can you tell me about this?
[meta]: **loading project overview info**
[meta]: Hello new visitor, here you go:
```

# About AURA

What is this about? You probably already know. If not, here are some links
that explain in more detail what the community radio stations in .AT land and
.DE land are which call themselves _Freie Radios_ (literally it would be
translated to _free radios_, but as the thing with _free_ here is the same
as with [free software](https://en.wikipedia.org/wiki/Free_and_open-source_software#Free_software)
in general).

Unfortunately most of those links are in German language as our constituency
is located primarily in Austria and German. We will provide a short intro
in English language as soon as we find some time.

About _Freie Radios_:

- [http://freie-radios.at](http://freie-radios.at) - Austrian association of community radio stations
- [http://freie-radios.de](http://freie-radios.de) - German association of community radio stations
- [https://cba.fro.at](https://cba.fro.at) - Podcast platform of Austrian community radio stations
- [https://freie-radios.net](https://freie-radios.net) - Audio portal of German community radio stations
- [https://amarceurope.eu](https://amarceurope.eu) - European association of community radio stations

**And what is this here now?**

_AuRa_ is a suite of radio management, programme scheduling and play-out
automation software that fits the needs of free radio stations. The initiative
took of in Austria, where several stations are still using and depending on
_Y.A.R.M._ (which is just yet another radio manager). _Y.A.R.M._ was an
awesome project that provided a software which was tailored to how
community radio stations create their diverse programmes. Unfortunately it
is also a piece of monolithic Java code that has come into the years.
Also it never really took of as a free software project and was depending on
a single developer. Today nobody really wants to touch its code anymore.

Now we urgently need something new, and all those other solutions out there
(FLOSS as well as commercial) do not really fulfill all our requirements.
Therefore we decided to pool our resources and develop something new, while
reusing a lot of work that has already been done at one or another station.
