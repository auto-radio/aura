# Release Notes

Here you find the release notes including all changes.

## [1.0.0-alpha2] - 2023-06-29

The release of **Alpha 2 - _Precise Pheasant_** ships with plenty of essential improvements, both under
the hood and on the user interface level.

The _Data Model_ has been extended and refined to provide a foundation for the requirements of
participating radios. Additional *recurrence rules* have been added as part of the default
configuration.

The *User Interface* has undergone a facelift and has been updated with features and enhancements,
especially in the calendar and show view.

Administrators are now served with log files for all services. In order to avoid permission errors,
also re-run the `make aura-user.add` command. This now creates directories for `logs` and `audio`
with correct permissions for the `aura` user.

**Breaking Changes:**

- Some configuration settings for the *Docker Compose* bundles have been changed. Either adapt
  your current `.env` file accordingly or start with a fresh one.
- The API has been improved to follow best practices. This includes the naming of parameters
  and properties.

**Deprecated:**

- Nested API endpoints in Steering and Tank are now deprecated. Try to migrate to the non-nested
  version of an endpoint and report back if you are facing issues.

For more details, check out [api.aura.radio](https://api.aura.radio) and the detailed changelog below.

### Added

- aura: Add development target `make dev.merge-changelog` to create a merged changelog, helping to prepare release notes.
- aura: Add release notes section at [docs.aura.radio](https://docs.aura.radio)
- aura: Add Tank Cut & Glue API to [api.aura.radio](https://api.aura.radio/).
- aura: Add default [Audio Store](https://docs.aura.radio/en/latest/administration/deployment-preparation.html#setting-up-the-audio-store) directory `/var/audio/import` used for audio file imports via filesystem (aura#172).
- aura: Add default setting for `AURA_AUDIO_STORE_RECORDINGS` to the playout `sample.env` file.
- aura: Add audio targets for the Makefile to start and disable PulseAudio.
- aura: Add optional engine-recorder profile to aura-playout.
- aura: Add an log file for NGINX, aura-web (#147).
- aura: Documentation: Add more details to the [Radio Station Administration](https://docs.aura.radio/en/latest/user/station-administration.html) chapter.
- aura: Documentation: Add chapter on deleting and deactivating users in the User Guide.
- aura: Documentation: Add more info on switching between releases to the Administration Guide.
- aura: Add directories to `logs` and `audio` to avoid permission errors (#209)
- steering: `Image` concrete model to handle all instances.
- steering: API endpoint `/api/v1/images/` to add, update and delete images.
- steering: Add `Host` admin
- dashboard: a new show selector to the navbar (#122, f9762d85).
- dashboard: a new image picker that can be used to upload new and select existing images (#89, #139, e0fd0a76, b335d801, 57e1e10f).
- dashboard: ability to add internal notes to a show (#146, 319cf540).
- dashboard: information on empty slots between shows and a progress indicator for the currently playing show
- dashboard: to the calendar day-view (#155, 9ac48a55, 2a68a7ac).
- dashboard: a progress indicator for the currently playing show to the calendar week-view (#156, b52672dd).
- dashboard: ability to add and remove individual contributors to the show notes based on the show owners (#170, #146, aa707763).
- dashboard: ability to add tags to show notes (#146, 560d8dda).
- engine: API responses from Steering and Tank are now cached in `cache_dir`
- engine-api: Test coverage (`make coverage` #40)
- engine-api: Badge displaying coverage result (#40)

### Changed

- aura: Update sample configuration files for services in `config/service/sample-config`.
- aura: Rename environment variable `AURA_ENGINE_SERVER_TIMEOUT` to `AURA_ENGINE_SERVER_TIMEOUT` in playout `sample.env`.
- aura: Increase default value for `AURA_ENGINE_SERVER_TIMEOUT` in playout `sample.env`, to avoid malfunctions when idle (aura#165).
- aura: Documentation: Update Contribution Guidelines and Coding Conventions in the Developer Guide.
- aura: Documentation: Extend section for developers on performing releases.
- aura: Change `/trackservice` endpoints to `/engine/api/v1/trackservice` and `/engine/api/v1/trackservice/current` according to API namespace conventions (aura#190).
- steering: The `Host`, `Note`, `Show` models & serializers reference the new `Image`.
- steering: The `logo` field in the `Show` model is a reference to `Image`.
- steering: The "conflict resolution" methods of `Schedule` are now service functions.
- steering: Update all APIs to return attributes / properties in camelCase notation (aura#141)
- steering: Use `_id` suffix for all object reference in REST APIs (aura#166)
- steering: Use blank strings instead of nullable strings in REST APIs (aura#167)
- steering: Upgrade Poetry dependencies and Django to the next LTS version (steering#137)
- dashboard: The timeslot overview has been fully revised (4ef88279).
- dashboard: The timeslot show notes editor has been fully revised (#141, 57e1e10f).
- dashboard: Past schedules are now hidden in the show manager (#120, 8cd743de).
- dashboard: Schedules are now shown with their respective start and end date (#121, f9dc6fef).
- dashboard: The calendar day-view has been fully revised (#155, 9ac48a55, 2a68a7ac).
- dashboard: The slots in the calendar week-view have been from 30 to 15 minutes to make it easier to
- dashboard: create shorter slots (#151, 3fc47383).
- dashboard: Very short timeslots in the calendar week-view now take up less space (#151, 3fc47383, bfe7f813).
- dashboard: Show start and end time in calendar tooltip if the event is too short to render it as content (#173, cf5ac12e).
- dashboard: The show manager interface for editing the show details has been overhauled, changing the overall appearance
- dashboard: and the way some of the fields are edited (db7dc49d).
- dashboard-clock: Provide properties in API schemas in CamelCase notation (aura#141)
- engine: Provide properties in API schemas in CamelCase notation (aura#141)
- engine-api: Make properties in API schemas in CamelCase notation (aura#141)
- engine-api: Avoid deprecation warning by replacing JSON encoder (#35)
- engine-api: Change HTTP status codes (204 No Content) for successful but empty POST/PUT responses, adapt tests (#5)
- engine-core: Make properties in API schemas in CamelCase notation (aura#141)
- engine-core: Configuration: Renamed environment variable `AURA_ENGINE_SERVER_TIMEOUT` to `AURA_ENGINE_SERVER_TIMEOUT`
- engine-core: and configuration setting `telnet_server_timeout` to `server_timeout`.
- engine-core: Configuration: Increase default value for `server_timeout`, to avoid malfunctions when idle (aura#165)

### Deprecated

- aura: Nested API endpoints in Steering and Tank are now deprecated. Compare the specification at api.aura.radio

### Removed

- steering: The abstract `ModelWithImageFields` from the program models.
- steering: The `ThumbnailsMixin` from the program serializers.
- steering: The abstract `ModelWithCreatedUpdatedFields` from the program models.
- dashboard: Non-functional nav button to broadcast management on show detail page (#133, e4083e66).
- engine: Remove mail service as it will be replaced by Prometheus monitoring (engine#109)

### Fixed

- aura: Fix an issue where `make aura-user.add` causes an unrelated Python error, raised from another command (#183).
- aura: Fix the location of the Docker Compose bundles in the README.
- aura: Fix an issue where `/trackservice` was not reachable (#159).
- aura: Fix an issue where some settings in the `.env` and `.yml` where not applied correctly (#160).
- aura: Fix an issue with the `aura-playout` targets in the Makefile (#168).
- aura: Documentation: Replace old "meta" repository references with "aura".
- steering: use kebab-case in URLs
- steering: don’t output invalid PPOI format
- steering: note image should allow null values
- steering: don’t force REST API consumers to set `repetition_of` for timeslots
- steering: The timeslot generation leaves the `last_date` unset if it is null.
- steering: Note.tags attribute not updated via API (steering#157)
- steering: make subtitle field in Category a CharField
- dashboard: Navigation would sometimes freeze, after an application error in the playlists view (#132, e4083e66).
- dashboard: Calendar would sometimes crash if playlist data is missing (#132, a3e8e3b0).
- dashboard: Switching between the weekand day-view in the calendar no longer squishes the week-view (#153, e28eb4f7).
- dashboard: The calendar uses entire viewport height now (#154, 4c90f277).
- dashboard: Users are now able to create a timeslot in a calendar week slot that has already started (#167, 3d4e9b28).
- dashboard: HTML in the show title, short description and description is now correctly rendered (#129, 649b4c0b).
- engine: Fix an issue where the Engine version is displayed in the logs wrongly
- engine-api: Fix an issue where the configuration file is overwritten in the `make init.dev` target
- engine-api: Fix an issue where the Docker Compose healthcheck failed (#39)
- engine-api: Fix broken endpoint `/trackservice` (aura#185)
- engine-core: Telnet server sample config not working (engine-core#45).
- engine-core: Extend and improve output for `make help`.
- engine-recorder: Fixed a bug where the recorder would not start when trying to record a web stream (aura#182)

## [1.0.0-alpha1] - 2023-03-02

Initial release.
