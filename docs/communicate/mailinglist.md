## Mailinglist

We sporadically share newsletters at the `users (at) aura.radio` Mailinglist.

### Subscribing to the list

In order to subscribe send a mail with following content to `majordomo (at) aura.radio`:

```txt
subscribe users-aura-radio
```

Then follow the instructions for confirmation in the reply mail. Alternatively you can message
the development team directly via `dev (at) aura.radio`, and we will add you to the list.

### Unsubscribing from the list

In order to unsubscribe send this to `majordomo (at) aura.radio`:

```txt
unsubscribe users-aura-radio
```