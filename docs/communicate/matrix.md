## Matrix

You can get in touch and contribute via [Matrix](https://matrix.org/) and
[Element](https://element.io/). This is our main communication channel.

Join the discussion at our Matrix Space and its main channels:

- [AURA Matrix Space](https://matrix.aura.radio)
  - [AURA-general](https://matrix.to/#/#AURA-general:freie-radios.de)
  - [AURA-dev](https://matrix.to/#/#AURA-dev-meta:freie-radios.de)
  - [AURA-support](https://matrix.to/#/#aura-support:freie-radios.de)
  - [AURA-notifications](https://matrix.to/#/#AURA-notifications:freie-radios.de)
  - [AURA-offtopic](https://matrix.to/#/#AURA-offtopic:freie-radios.de)
