# Architecture

Find details on the AURA Architecture here.

## Architecural principals

Some of our core organisational and architectural requirements for AURA are:

- **modular architecture**: the whole suite should be made up of modular components which could be exchanged with other custom components
- **transparent API**: every component shall provide a well-documented API through which other components can interact with it, ideally as a REST API.
- **reuse of existing components**: we do not want to reinvent the wheel. Several stations already developed single components as free software and we can adapt and build on those
- **modern frameworks**: we do not code from scratch but use modern application development frameworks which provide maintainability as well as security

```{admonition} Outdated diagram
:class: tip

The following component diagram doesn't reflect all the details of the current implementation. It will be updated at some point.
```

![Diagram illustrating the AuRa components](https://gitlab.servus.at/aura/aura/raw/main/assets/images/components.png 'AuRa Components')

## Diagrams

### Network Diagram

Check out the provided [Deployment Scenarios](../administration/deployment-scenarios.md) on ideas how the individual projects can be integrated within your infrastructure.

### Data Model

#### Simplified Data Model

This data model is an abstraction of the main entities used by Steering and Tank.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-data-model-simple.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-data-model-simple.png" width="800" /></a>

#### Steering Data Model

This is the Steering data model as of November 2023. Parts of Django and other applications that are not directly involved were omitted.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering_data_model.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering_data_model.png" width="800" /></a>

### Tank Data Model

This is the Tank data model as of November 2023.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/tank_data_model.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/tank_data_model.png" width="800" /></a>

## Conflict Resolution for the scheduling timetable

Check out the [Conflict Resolution Documentation](misc/conflict-resolution.md) page.
