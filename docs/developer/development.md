# Development

## Coding Conventions and Guidelines

Here you find an overview of our conventions on coding and version control.

Also have a look at our [Contribution Guidelines](../contribute/contributions.md).

### git

- We use [GitHub Flow](https://docs.github.com/en/get-started/quickstart/github-flow)
- Keep the `main` branch stable, as releases are derived from it. When contributing a breaking change, like API changes, inform the team about the intended merge.
- When contributing code, create a _Merge Request_, and assign the repository maintainer for review. Do not _squash commits_ when performing the merge.
- We write changelogs in the [keep a changelog](https://keepachangelog.com/) format. Remind yourself to update the `CHANGELOG.md` before committing code. Remember that changelogs are meant for humans and should reflect the end-user value. When [doing a release](releases.md) we use the changelog as our _Release Notes_. When providing code via a _Merge Request_ the maintainer will remind you to update the changelog too :-)
- Avoid crunching commits and rebasing; set `git config pull.rebase false` to use _recursive_ as your default merge strategy
- We use [_conventional commits_](https://www.conventionalcommits.org/)
- Mention related ticket IDs where applicable, like `#123` or `play#123` when cross-referencing between repositories. For example you can close tickets by using `closes #123`.
- Use [atomic commits](https://www.freshconsulting.com/insights/blog/atomic-commits/)

### Code

#### Python

We use [Black](https://github.com/psf/black/) with default settings, enforced by [Flake8](https://flake8.pycqa.org/en/latest/).

We use the default settings, except for a maximal line-length of 99 characters. If you are using
a Black IDE integration, think about adapting its settings.

For code documentation we use the [Flake8 Docstrings](https://pypi.org/project/flake8-docstrings/) extension with [Google-style formatting](https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings) enabled (`docstring-convention=google`).

#### ECMAScript and TypeScript

We use [ESLint](https://eslint.org/) as the common Linter. When your code is based on [VueJS](https://vuejs.org/) or [Svelte](https://svelte.dev/), install the official IDE extensions. These extensions provide additional style checks and auto-formatting options.

### API

We utilize an _API-first_ approach. APIs are specified using OpenAPI 3. Find the API at [api.aura.radio](https://api.aura.radio).

All the main aspects are documented within the spec. In some cases you may need some additional documentation in the docs. For example the [API: Schedules and conflict resolution](misc/conflict-resolution.md) document can be found in "_Developer Guide -> Misc_".

```{admonition} API-first
:class: tip

At the moment only [Engine API](https://api.aura.radio/engine) is based on API-first. [Steering API](https://api.aura.radio/steering) and [Tank API](https://api.aura.radio/tank) are momentarily generated out of the code base.
```

### Testing

When providing new features or refactoring code, please provide test cases. We do not expect
100% test coverage but are aiming for having the most important usage scenarios covered by
automated CI/CD test suites.

#### Auto Deployment

For all components, feature branches automatically build Docker containers and push them to the [gitlab-registry](https://gitlab.servus.at/groups/aura/-/container_registries).
To roll out new features, the [Compose-File](https://gitlab.servus.at/aura/aura/-/blob/main/config/aura-web/docker-compose.auto-deployment.yml),
must be set to the corresponding tag of the feature branch. 
After the commit, the version will be rolled out automatically.

### Documentation

The general documentation is located in `aura/docs` and hosted at [docs.aura.radio](https://docs.aura.radio). When working on any component, also check if this documentation has to be updated or extended.

## Developer Installation

For Development the native installation as outlined in the `README` of individual [repositories](https://code.aura.radio) recommended.

```{admonition} Docker Compose Deployments
:class: tip

For production we highly recommend to run AURA using Docker and Docker Compose as outlined in the [Administration Guide](../administration/index.md). But also as a developer you can benefit from the ease of an Docker Compose installation. For example when developing some Engine feature, you may want to run AURA Web with Docker Compose while testing.
```

### Prepare your Development Environment

It is recommended to clone all projects for example in such folder structure:

```bash
~code/aura/aura
~code/aura/steering
~code/aura/dashboard
~code/aura/engine
~code/aura/engine-api
~code/aura/engine-core
...
```

### Order of configuration

After that, you need to configure the projects in following order:

**Web Projects**

1. [Steering](https://gitlab.servus.at/aura/steering) - Administration interface for schedules and programme information.
2. [Dashboard](https://gitlab.servus.at/aura/dashboard) - Frontend to manage schedules, program info and audio files.
3. [Tank](https://gitlab.servus.at/aura/tank) - Upload, pre-processing and storage of the audio files.
4. ...

**Play-out Projects**

1. [Engine Core](https://gitlab.servus.at/aura/engine) - Playout-engine to deliver the actual radio to the audience.
2. [Engine API](https://gitlab.servus.at/aura/engine) - API Server to provide playlogs and information for the studio clock.
3. [Engine](https://gitlab.servus.at/aura/engine) - Scheduling and remote control for the playout-engine.
4. ...

### Configuring the OpenID Clients

Dashboard and Tank authenticate against Steering using OpenID. We use [OpenID Connect (OIDC)](https://openid.net/connect/) to implement OpenID.

[Check out the OIDC configuration](misc/oidc-client-config.md) page, on how to get them talk to each other.

## Component Documentation

For more detailed documentation read the `README` files in the individual repositories.

You get an overview of all repositories at [code.aura.radio](https://code.aura.radio).
