# Release Management

## Semantic Versioning

Release names are defined according to the [SemVer 2.0.0](https://semver.org/) versioning scheme.

```{admonition} Semantic versioning of releases in CI/CD Pipelines
:class: tip

The chosen git tag will be the release name and the version of the Docker Image. Therefore the
pipeline script will enforce it to conform to [Semantic Versioning 2.0.0](https://semver.org/).
```

## Keep a Changelog

For changelogs we use the format suggested by [Keep a Changelog](https://keepachangelog.com/).
Try to follow best practices when writing the changelog. But remember, changelogs are for humans.
So do apply best practices when compiling your changelog.

The general AURA changelog is located in `CHANGELOG.md`.

Additionally, all individual service repositories hold their own changelog file. At each release
they are bundled in the `aura/changelog` directory.

## Current Release

You can find available releases at [releases.aura.radio](https://releases.aura.radio) or by
executing `git tag`.

Find releases of individual services in the [Gitlab](https://code.aura.radio) repository page
under _Deployments > Releases_.

## Release Workflow

### Releasing a service

This release step is **done by the repository maintainers**.

To perform a service release do:

1. Bump the version in the file holding the current version. Commit the version update.
2. Release the version by running `make release`. This command tags and pushes the current branch.

As soon a version tag is pushed, the _CI/CD Pipeline_ performs these steps:

- Create a GitLab release with the provided `CHANGELOG.md` as its release notes.
- Pushes the image to [Docker Hub](https://hub.docker.com/u/autoradio).
- Build the _Docker Image_ and automatically tags the release with  `latest` and `<release version>`.

### Releasing the AURA software bundle

The release of a complete software bundle is triggered from within the `aura` repository and
**performed by the release manager**.

Before the bundle can be released, the repository maintainers are informed about a <u>code-freeze</u>
in a timely manner and asked to review their changelogs and do releases of their individual services
(see 'Releasing a service' above).

Before proceeding also review the main AURA changelog in `aura/CHANGELOG.md`.

#### Preparing the release

1. Create a new branch for the release: `git checkout -b %VERSION%`.
2. In `aura/CHANGELOG.md` change the version identifier `[Unreleased]` to the release version and
   add the current date.
3. Bump the version of the `aura` repository with `poetry version <version>`.
4. Update the versions of referenced services in the _Docker Compose_ `config/<docker compose>/sample.env`
   files and in `.env.version`.
5. Update the configuration files and changelogs by running

   ```
   make dev.prepare-release
   ```

   This command pulls all configuration files and changelogs from the individual repos identified
   by the versions in `.env.version`. The changlogs are stored `.tmp` and are merged into a file
   named `merged-CHANGELOG-%AURA_VERSION%.md`
6. Commit and push the branch.

Now inform the testers to check-out and test the given branch.

#### Performing the release

After the **testers inform the product owner and development team**, that the _release criteria_
is met, we can proceed.

In case developers have done any last minute bugfixes and re-released their services, ensure all
the configuration and changelog files are still up-to-date by executing

```bash
make  dev.prepare-release
```

once more.

1. Take the contents of `.tmp/merged-CHANGELOG-%AURA_VERSION%.md` and paste it into a new section
   in `docs/release-notes.md`.
2. Now we have to start thinking: What should be in the header of the release notes? Comprehend
   what this release delivers, by checking the roadmap and contents of the changelog below.
3. Review and commit the changes.
4. Run

   ```bash
   make dev.release
   ```

5. Check out `main` and merge everything changed, except the version updates. They should remain
   as `unstable` for services and `Unreleased` for `AURA_VERSION`. Alternatively cherry-pick the
   relevant commits.
6. Update `CHANGELOG.md` with a new template section for the next release.
7. Push the changes to `main`.
8. Delete the branch used for release preparation.