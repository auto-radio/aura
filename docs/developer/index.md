# Developer Guide

This guide holds general information for AURA architecture and development.

```{toctree}
---
caption: Table of contents
maxdepth: 1
glob: True
---

architecture
development
releases
modules/index
misc/index
```
