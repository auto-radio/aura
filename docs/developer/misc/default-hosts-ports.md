
# Default hosts and ports

Here you find the default hosts and ports for all AURA applications.

## Development environment

|    Component    |      Host:Port       |         Description           |
|-----------------|:--------------------:|:------------------------------|
| steering        | localhost:8080       | Django Development Server     |
| dashboard       | localhost:8000       | VueJS Development Server      |
| dashboard-clock | localhost:5000       | Svelte Development Server     |
| tank            | localhost:8040       | Go Development Server         |
| engine          | localhost:1337       | Network Socket for Liquidsoap |
| engine-api      | localhost:8008       | Werkzeug Development Server   |
| engine-core     | localhost:1234       | Liquidsoap Telnet Server      |
| play            | localhost:5000       | Svelte Development Server     |