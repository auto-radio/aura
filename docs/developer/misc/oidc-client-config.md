# OpenID Client Configuration

AURA is using [OpenID](https://en.wikipedia.org/wiki/OpenID) for authentication and authorizing
access to restricted API endpoints.

More specifically we are using [OpenID Connect (OIDC)](https://openid.net/connect/) for the OpenID handshakes.

[Steering](https://gitlab.servus.at/aura/steering) is the central OpenID provider. All applications requesting access,
need to get an authorization from Steering.

Those applications are called _OIDC clients_.

## Required OIDC Clients

In order to properly setup AURA, you'll need to configure OpenID clients for Dashboard and Tank.

The registration and configuration steps below use [the default hosts & ports](default-hosts-ports.md).

In case of a [Production Deployment](../../administration/index.md) you'll probably have substitutions like following:

- Steering: _localhost:8080 → aura-host.org/admin_
- Dashboard: _localhost:8000 → aura-host.org_
- Tank: _localhost:8040 → aura-host.org/tank_

## Registering clients at Steering

### Registering OIDC clients on the command-line

First navigate to your Steering project location.

1. Create an RSA Key

```bash
$ poetry run ./manage.py creatersakey
```

2. Create OIDC client for Dashboard

```bash
$ poetry run ./manage.py create_oidc_client dashboard public -r "id_token token" -u https://localhost:8080/oidc_callback.html -u https://localhost:8080/oidc_callback_silentRenew.html -p https://localhost:8080/
```

**Important:** Remember to note the client id and secret for the configuration section below.

1. Create OIDC client for Tank

```bash
$ poetry run ./manage.py create_oidc_client tank confidential -r "code" -u https://localhost:8040/auth/oidc/callback
```

**Important:** Remember to note the client id and secret for the configuration section below.

### Registering OIDC clients via the admin interface

Follow these three steps to register Dashboard and Tank in the OpenID admin section of Steering.

#### Create an RSA Key

In the admin interface navigate to _OpenID Connect Provider_ and _generate a RSA Key_.

#### Create OIDC client for Dashboard

Here you'll need to choose following settings:

```
Client Type: Public
Response Type: id_token token (Implicit Flow)
JWT Algorithm: RS256
Require Consent?: No
Reuse Consent?: Yes
```

And enter these redirect URLs:

```
http://localhost:8080/static/oidc_callback.html
http://localhost:8080/static/oidc_callback_silentRenew.html
```

Note, that these URLs have to match exactly the
ones you configure in your `.env.development` or `.env.production` files here
in the dashboard source. This also means that if you use `localhost` in steering,
you must not put `127.0.0.1` or any aquivalent in your dashboard config, but
use exactly the same string (and vice versa).

Note the _Client ID_ to use in your Dashboard config file.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering-openid-dashboard.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering-openid-dashboard.png" width="800" /></a>

```{admonition} TODO
:class: alert

Replace image with a current screenshot of Steering
```

#### Create OIDC client for Tank

Here you'll need to choose following settings:

```
Client Type: Confidential
Response Type: code (Authorization Code Flow)
JWT Algorithm: RS256
Require Consent?: No
Reuse Consent?: Yes
```

And enter that redirect URL:

```
http://localhost:8040/auth/oidc/callback
```

Note the _Client ID_ and secret to use in your Tank config file.

<a href="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering-openid-tank.png"><img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/steering-openid-tank.png" width="800" /></a>

```{admonition} TODO
:class: alert

Replace image with a current screenshot of Steering
```

## Setting the client configuration

When configuring a client, always remind yourself to use the actual hostname. When using the IP address for OIDC redirect URLs
you might get unexpected behaviour or being unable to authenticate at all.

### Configuring Dashboard

In the Dashboard folder, edit your `.env.production` or `.env.development` respectively, and carefully review
if these URLs are matching the ones in the the Steering client settings. These URLs should match your Dashboard host:

```ini
VUE_APP_API_STEERING_OIDC_REDIRECT_URI = http://localhost:8080/oidc_callback.html
VUE_APP_API_STEERING_OIDC_REDIRECT_URI_SILENT = http://localhost:8080/oidc_callback_silentRenew.html
```

Then set the client id and secret, which you noted from the previous step:

```ini
VUE_APP_OIDC_CLIENT_ID = %YOUR_ID%
```

Additionally, confirm that your configured Steering URL and port is also matching the instance Steering is running at:

```ini
VUE_APP_API_STEERING_OIDC_URI = http://localhost:8000/openid
```

### Configuring Tank

In the Tank configuration file `tank.yaml` replace `${OIDC_CLIENT_ID}` and `${OIDC_CLIENT_SECRET}` with your client ID and secret, or set the environment variables accordingly.

Also review the given URLS.

```yaml
oidc:
  issuer-url: http://localhost:8000/openid
  client-id: ${OIDC_CLIENT_ID}
  client-secret: ${OIDC_CLIENT_SECRET}
  callback-url: http://localhost:8040/auth/oidc/callback
```
