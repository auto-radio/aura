
# Bug Reports

Tell us about your problems!

But please use the bug report template below.

## Contact us via Matrix

You can find as on [Matrix](https://matrix.aura.radio) which is also our primary channel for communication.

## Create a ticket on GitLab

If you don't have an Gitlab account, you can [sign up here](https://gitlab.servus.at/users/sign_up).

1. Head over to our GitLab instance at [code.aura.radio](https://code.aura.radio).
2. Search across all projects for existing tickets relevant to your issue.
3. If something is available already, vote for it and place your comment.
4. If nothing is available, first choose the relevant project. If you are unsure about the project, use the [`aura` repository](https://gitlab.servus.at/aura/aura/-/issues). Then file a new ticket.

## Bug Reporting Template

It's helpful if you structure your report similar to that template.

```markdown

# Title

*summary describing your issue*

## Steps to Reproduce

 1. ...
 2. ...
 3. ...

## Expected Result

...

## Actual Result

...

## Logs & configuration

- Contents of your `.env` or other configuration files. Keep in mind to remove any sensitive data like password, as the report will be visible publicly.
- Any errors in your browser console. In Firefox you can reach it by pressing `CTRL + SHIFT + K`, in Chrome or Chromium via `CTRL + SHIFT + J`.
- In case you are using your own Docker Compose override (`docker-compose.override.yml`), please share the file contents.
- The output of `docker-compose ps --all`, ensuring all services are started successfully.

## Environment

*optional details on your environment*