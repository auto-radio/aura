# Contributing to AURA

## Code of Conduct

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)

We inherit the [Contributor Covenant](code_of_conduct.md).

## How can I contribute?

You don't need to be a developer to contribute to the project.

- Join the [AURA Matrix Space](https://matrix.to/#/#aura:freie-radios.de).
- Check out the source code and try AURA for yourself.
- [Create bug reports, feature requests and provide thoughts in GitLab](bug-reports.md).
- Become an active developer or maintainer. To do so, check out the [Developer Guide](../developer/index.md), especially the [Coding Conventions](../developer/development.md).
- Provide sponsorship. We are happy to list you on the front page.

## Contribution Guidelines

- Read up on our [Developer Guide > Coding Conventions](../developer/development.md).
- When contributing code, create a _Merge Request_ and assign the repository maintainer for review.
- We write changelogs in the [keep a changelog](https://keepachangelog.com/) format. Update the `CHANGELOG.md` before committing code. Remember that changelogs are meant for humans and should reflect the end-user value.
- In case of big changes affecting several services and APIs it might be good to discuss that with the team beforehand. Think about doing a test deployment with all required services.

## Contributors

- Code contributors can be found in the `git` logs.
- Martin Lasinger from [Freies Radio Freistadt]() designed the AURA logos and icons.
- The foundation of Steering is based on [Radio Helsinki](https://helsinki.at/)'s [PV Module](https://git.helsinki.at/pv.git/) by Ernesto Rico Schmidt.
- The foundation of Engine is based on [Comba](https://gitlab.janguo.de/freie-radios/comba), by Michael Liebler and Steffen Müller.

## Partners and Sponsorship

Current partners

- [Radio Orange 94.0 - Verein Freies Radio Wien](https://o94.at/)
- [Radio Helsinki - Verein freies Radio Steiermark](https://helsinki.at/)
- [Radio FRO - Freier Rundfunk Oberösterreich](https://www.fro.at/)
- [Freies Radio Wüste Welle](https://wueste-welle.de/)
- [Freies Radio Freistadt](https://www.frf.at/)
- [Radio free FM](https://freefm.de/)
- [FREIRAD Freies Radio Innsbruck](https://freirad.at)

Previous partners and sponsors

- [Radiofabrik - Verein Freier Rundfunk Salzburg](https://radiofabrik.at/)

## Licensing

By contributing your code you agree to these licenses and confirm that you are the copyright owner of the supplied contribution.

## License

- Logos and trademarks of sponsors and supporters are copyrighted by the respective owners. They should be removed if you fork this repository.
- All source code is licensed under [GNU Affero General Public License (AGPL) v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).
- All other assets and text are licensed under [Creative Commons BY-NC-SA v3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/).

These licenses apply unless stated differently.
