# Sphinx Config
from datetime import datetime

project = "AURA - Automated Radio"
author = "The AURA Team"
copyright = f"2017-{datetime.now().year}, the AURA Team"

# Base theme
html_theme = "sphinx_rtd_theme"

# These folders are copied to the documentation's HTML output
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = ["aura.css"]


html_title = "AURA - Automated Radio"
root_doc = "index"
html_logo = "../assets/images/aura-logo-grayscale-trans.png"
html_favicon = "../assets/images/aura_fav96x96.png"
html_theme_options = {
    "prev_next_buttons_location": "bottom",
    "navigation_depth": 5,
    "navigation_with_keys": True,
    "globaltoc_depth": 5,
    "globaltoc_maxdepth": 5,
    "globaltoc_includehidden": True,
    "stickysidebar": True,
}
extensions = [
    "myst_parser",
]
source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}
