.DEFAULT_GOAL := help
-include scripts/make/audio.Makefile
include .env.version
export

help::
	@echo
	@echo "---------------------------------------------------------------------------------------"
	@echo "AURA production targets:"
	@echo
	@echo "    aura-user.add                - create user 'aura'. Owns '/opt/aura'."
	@echo "    aura-user.rm                 - delete user 'aura'"
	@echo
	@echo "    aura-web.init                - init docker compose 'aura-web'"
	@echo "    aura-web.update              - pull latest images for 'aura-web'"
	@echo "    aura-web.up                  - start docker compose 'aura-web'"
	@echo "    aura-web.down                - stop docker compose 'aura-web'"
	@echo "    aura-web.permissions-update  - update permissions for '/opt/aura'"
	@echo
	@echo "    aura-playout.update          - pull latest images for 'aura-playout'"
	@echo "    aura-playout.up              - start docker compose 'aura-playout'"
	@echo "    aura-playout.down            - stop docker compose 'aura-playout'"
	@echo
	@echo "---------------------------------------------------------------------------------------"
	@echo "AURA development targets:"
	@echo
	@echo "    dev.init                     - init development environment"
	@echo "    dev.spell                    - check spelling of text"
	@echo "    dev.clone-config             - update sample configuration from service repos"
	@echo "    dev.clone-changelog          - update service changelogs"
	@echo "    dev.merge-changelog          - create a merged changelog"
	@echo "    dev.prepare-release          - prepare the release (update config, merge changelog)"
	@echo "    dev.release                  - tags und pushes the release"
	@echo
	@echo "---------------------------------------------------------------------------------------"
	@echo "AURA audio targets:"
	@echo
	$(call audio_help)

# Settings

AURA_GID ?= "2872"
AURA_UID ?= "2872"
AURA_HOME ?= "/opt/aura"

# Targets

aura-user.add::
	@if [ `id --user ${AURA_UID}` = ${AURA_UID} ] \
	&& [ `id --group ${AURA_GID}` = ${AURA_GID} ]; then\
		echo "The user aura (${AURA_UID}:${AURA_GID}) already exists.";\
	else \
		sudo groupadd --gid ${AURA_GID} aura;\
		sudo useradd --gid ${AURA_GID} --no-user-group --uid ${AURA_UID} --home-dir ${AURA_HOME} --no-create-home aura;\
		sudo passwd aura;\
	fi
	mkdir -p "${AURA_HOME}/audio/source" "${AURA_HOME}/audio/import" "${AURA_HOME}/audio/fallback" "${AURA_HOME}/audio/recordings/block" "${AURA_HOME}/logs/tank" ${AURA_HOME}/logs/nginx" ${AURA_HOME}/logs/letsencrypt"
	sudo chown -R ${AURA_UID}:${AURA_UID} ${AURA_HOME}
	chmod -R a+rwx ${AURA_HOME}/logs/
	@echo
	@echo "Successfully created user 'aura' and now '${AURA_HOME}' feels like home."

aura-user.rm:
	sudo deluser aura
	sudo delgroup aura

aura-web.init::
	cd config/aura-web && docker compose run --rm steering initialize

aura-web.update::
	git pull
	cd config/aura-web && docker compose pull

aura-web.up::
	cd config/aura-web && docker compose up -d

aura-web.down::
	cd config/aura-web && docker compose down

aura-web.permissions-update::
	mkdir -p "${AURA_HOME}/audio/source" "${AURA_HOME}/audio/import" "${AURA_HOME}/audio/fallback" "${AURA_HOME}/audio/recordings/block" "${AURA_HOME}/logs/tank" ${AURA_HOME}/logs/nginx" ${AURA_HOME}/logs/letsencrypt"
	sudo chown -R ${AURA_UID}:${AURA_UID} ${AURA_HOME}
	sudo chown -R 101:${AURA_UID} ${AURA_HOME}/logs/letsencrypt ${AURA_HOME}/logs/nginx

aura-playout.update::
	git pull
	cd config/aura-playout && docker compose pull

aura-playout.up::
	cd config/aura-playout && docker compose up -d

aura-playout.down::
	cd config/aura-playout && docker compose down

dev.init::
	pip install poetry
	poetry install --no-interaction
	poetry run pre-commit autoupdate
	poetry run pre-commit install

dev.spell::
	poetry run codespell $(wildcard *.md) docs

dev.clone-config:: REPO_BASE := https://gitlab.servus.at/aura/
dev.clone-config:: BRANCH_STEERING := $(subst unstable,main,$(AURA_STEERING_VERSION))
dev.clone-config:: BRANCH_TANK := $(subst unstable,main,$(AURA_TANK_VERSION))
dev.clone-config:: BRANCH_TANK_CUT_GLUE := $(subst unstable,main,$(AURA_TANK_CUT_GLUE_VERSION))
dev.clone-config:: BRANCH_DASHBOARD := $(subst unstable,main,$(AURA_DASHBOARD_VERSION))
dev.clone-config:: BRANCH_DASHBOARD_CLOCK := $(subst unstable,main,$(AURA_DASHBOARD_CLOCK_VERSION))
dev.clone-config:: BRANCH_ENGINE := $(subst unstable,main,$(AURA_ENGINE_VERSION))
dev.clone-config:: BRANCH_ENGINE_API := $(subst unstable,main,$(AURA_ENGINE_API_VERSION))
dev.clone-config:: BRANCH_ENGINE_CORE := $(subst unstable,main,$(AURA_ENGINE_CORE_VERSION))
dev.clone-config:: BRANCH_ENGINE_RECORDER := $(subst unstable,main,$(AURA_ENGINE_RECORDER_VERSION))
dev.clone-config::
	curl ${REPO_BASE}steering/-/raw/${BRANCH_STEERING}/steering/sample_settings.py?inline=false > ./config/services/sample-config/steering.py
	curl ${REPO_BASE}tank/-/raw/${BRANCH_TANK}/contrib/for-docker.yaml?inline=false > ./config/services/sample-config/tank.yaml
	curl ${REPO_BASE}tank-cut-glue/-/raw/${BRANCH_TANK_CUT_GLUE}/config/for-docker.yaml?inline=false > ./config/services/sample-config/tank-cut-glue.yaml
	curl ${REPO_BASE}dashboard/-/raw/${BRANCH_DASHBOARD}/docker.env.production?inline=false > ./config/services/sample-config/dashboard.env
	curl ${REPO_BASE}dashboard-clock/-/raw/${BRANCH_DASHBOARD_CLOCK}/config/default-sample.env?inline=false > ./config/services/sample-config/dashboard-clock.env
	curl ${REPO_BASE}engine/-/raw/${BRANCH_ENGINE}/config/sample.engine.docker.ini?inline=false > ./config/services/sample-config/engine.ini
	curl ${REPO_BASE}engine-api/-/raw/${BRANCH_ENGINE_API}/config/sample.engine-api.docker.yaml?inline=false > ./config/services/sample-config/engine-api.yaml
	curl ${REPO_BASE}engine-core/-/raw/${BRANCH_ENGINE_CORE}/config/sample.engine-core.docker.ini?inline=false > ./config/services/sample-config/engine-core.ini
	curl ${REPO_BASE}engine-recorder/-/raw/${BRANCH_ENGINE_RECORDER}/config/for-docker.yaml?inline=false > ./config/services/sample-config/engine-recorder.yaml

dev.clone-changelog:: AURA_WORK_DIR := ./.tmp
dev.clone-changelog:: REPO_BASE := https://gitlab.servus.at/aura/
dev.clone-changelog:: BRANCH_STEERING := $(subst unstable,main,$(AURA_STEERING_VERSION))
dev.clone-changelog:: BRANCH_TANK := $(subst unstable,main,$(AURA_TANK_VERSION))
dev.clone-changelog:: BRANCH_TANK_CUT_GLUE := $(subst unstable,main,$(AURA_TANK_CUT_GLUE_VERSION))
dev.clone-changelog:: BRANCH_DASHBOARD := $(subst unstable,main,$(AURA_DASHBOARD_VERSION))
dev.clone-changelog:: BRANCH_DASHBOARD_CLOCK := $(subst unstable,main,$(AURA_DASHBOARD_CLOCK_VERSION))
dev.clone-changelog:: BRANCH_ENGINE := $(subst unstable,main,$(AURA_ENGINE_VERSION))
dev.clone-changelog:: BRANCH_ENGINE_API := $(subst unstable,main,$(AURA_ENGINE_API_VERSION))
dev.clone-changelog:: BRANCH_ENGINE_CORE := $(subst unstable,main,$(AURA_ENGINE_CORE_VERSION))
dev.clone-changelog:: BRANCH_ENGINE_RECORDER := $(subst unstable,main,$(AURA_ENGINE_RECORDER_VERSION))
dev.clone-changelog::
	mkdir -p ${AURA_WORK_DIR}
	cp CHANGELOG.md ./${AURA_WORK_DIR}/CHANGELOG-aura.md
	curl ${REPO_BASE}steering/-/raw/${BRANCH_STEERING}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-steering.md
	curl ${REPO_BASE}tank/-/raw/${BRANCH_TANK}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-tank.md
	curl ${REPO_BASE}tank-cut-glue/-/raw/${BRANCH_TANK_CUT_GLUE}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-tank-cut-glue.md
	curl ${REPO_BASE}dashboard/-/raw/${BRANCH_DASHBOARD}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-dashboard.md
	curl ${REPO_BASE}dashboard-clock/-/raw/${BRANCH_DASHBOARD_CLOCK}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-dashboard-clock.md
	curl ${REPO_BASE}engine/-/raw/${BRANCH_ENGINE}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-engine.md
	curl ${REPO_BASE}engine-api/-/raw/${BRANCH_ENGINE_API}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-engine-api.md
	curl ${REPO_BASE}engine-core/-/raw/${BRANCH_ENGINE_CORE}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-engine-core.md
	curl ${REPO_BASE}engine-recorder/-/raw/${BRANCH_ENGINE_RECORDER}/CHANGELOG.md?inline=false > ./${AURA_WORK_DIR}/CHANGELOG-engine-recorder.md

dev.merge-changelog:: AURA_WORK_DIR := ./.tmp
dev.merge-changelog::
	mkdir -p ${AURA_WORK_DIR}
	poetry run python scripts/merge_changelog.py

dev.prepare-release:: dev.clone-config
dev.prepare-release:: dev.clone-changelog
dev.prepare-release:: dev.merge-changelog
dev.prepare-release::
	@echo Release prepared.
	@echo
	@echo Now update 'docs/release-notes.md' with relevant information from the changelog generated in '.tmp'.

dev.release::
	$(eval VERSION := $(shell python3 -c 'import tomli; print(tomli.load(open("pyproject.toml", "rb"))["tool"]["poetry"]["version"])'))
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."
